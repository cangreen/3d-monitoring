<?php

use yii\db\Migration;

/**
 * Class m200312_105205_add_roles
 */
class m200312_105205_add_roles extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $am = Yii::$app->authManager;

    $superAdmin = $am->createRole('superadmin');
    $superAdmin->description = 'Суперадмин';
    $admin = $am->createRole('administrator');
    $admin->description = 'Администратор';
    $operator = $am->createRole('operator');
    $operator->description = 'Оператор';

    $perm = $am->createPermission('personal_area');

    if (!($am->add($superAdmin) && $am->add($admin) && $am->add($operator) && $am->add($perm))) {
      throw new \yii\console\Exception('Неудача');
    } else {
      $am->addChild($superAdmin, $perm);
      $am->addChild($admin, $perm);
      $am->addChild($operator, $perm);
      echo 'Роли добавлены';
    };
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $am = Yii::$app->authManager;
    $superAdmin = $am->getRole('superadmin');
    $admin = $am->getRole('administrator');
    $operator = $am->getRole('operator');
    $perm = $am->getPermission('personal_area');

    if (!($am->remove($superAdmin) && $am->remove($admin) && $am->remove($operator) && $am->remove($perm))) {
      throw new \yii\console\Exception('Неудача');
    } else {
      echo 'Роли удалены';
    };
  }
}
