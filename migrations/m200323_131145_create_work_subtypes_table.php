<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%work_subtypes}}`.
 */
class m200323_131145_create_work_subtypes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->createTable('{{%work_subtypes}}', [
        'id' => $this->primaryKey(),
        'name' => $this->string(32)->notNull()
      ]);

      $this->insert('work_subtypes', ['name' => 'МКД']);

      $this->insert('work_subtypes', ['name' => 'Территория']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%work_subtypes}}');
    }
}
