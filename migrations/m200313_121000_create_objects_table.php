<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%objects}}`.
 */
class m200313_121000_create_objects_table extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->createTable('{{%stations}}', [
      'id' => $this->primaryKey(),
      'number' => $this->integer(6)->notNull(),
      'name' => $this->string(128)->notNull(),
      'director_name' => $this->string(128)->notNull(),
      'director_surname' => $this->string(128)->notNull(),
      'director_patronymic' => $this->string(128)->notNull(),
      'phone' => $this->string(15)->notNull(),
      'email' => $this->string(128)->notNull(),
      'index' => $this->string(10)->notNull(),
      'city' => $this->string(128)->notNull(),
      'district' => $this->string(128)->notNull(),
      'region' => $this->string(128)->notNull(),
      'street' => $this->string(128)->notNull(),
      'house' => $this->string(16)->notNull(),
      'flat' => $this->string(5),
      'active' => $this->boolean()->notNull()->defaultValue(1),
      'created_at' => $this->integer(),
      'updated_at' => $this->integer(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropTable('{{%stations}}');
  }
}
