<?php

use yii\db\Migration;

/**
 * Class m200313_113657_add_permission_catalog
 */
class m200313_113657_add_permission_catalog extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {

    $am = Yii::$app->authManager;
    $perm = $am->createPermission('catalog');
    $am->add($perm);
    $admin = $am->getRole('administrator');
    $super = $am->getRole('superadmin');

    $am->addChild($admin, $perm);
    $am->addChild($super, $perm);

  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $am = Yii::$app->authManager;
    $perm = $am->getPermission('catalog');
    $am->remove($perm);
    return true;
  }

}
