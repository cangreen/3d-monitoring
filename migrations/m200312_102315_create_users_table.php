<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m200312_102315_create_users_table extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->createTable('{{%users}}', [
      'id' => $this->primaryKey(),
      'email' => $this->string(128)->notNull()->unique(),
      'password_hash' => $this->string(512)->notNull(),
      'auth_key' => $this->string(32)->notNull(),
      'password_reset_token' => $this->string()->unique(),
      'name' => $this->string(64),
      'surname' => $this->string(128),
      'patronymic' => $this->string(128),
      'phone' => $this->string(15),
      'active' => $this->boolean()->defaultValue(1),
      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropTable('{{%users}}');
  }
}
