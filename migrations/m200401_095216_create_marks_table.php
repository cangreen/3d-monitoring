<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%marks}}`.
 */
class m200401_095216_create_marks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%marks}}', [
            'id' => $this->primaryKey(),
            'checklist_id' => $this->integer()->notNull(),
            'criterion_id' => $this->integer()->notNull(),
            'value' => $this->tinyInteger()->notNull()
        ]);

        $this->addForeignKey('marks_fk1', 'marks', 'checklist_id', 'checklists', 'id', 'CASCADE');

        $this->addForeignKey('marks_fk2', 'marks', 'criterion_id', 'mark_criteria', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('marks_fk1', 'marks');

        $this->dropForeignKey('marks_fk2', 'marks');

        $this->dropTable('{{%marks}}');
    }
}
