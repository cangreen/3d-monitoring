<?php

use yii\db\Migration;

/**
 * Class m200320_094038_alter_users_add_role_name
 */
class m200320_094038_alter_users_add_role_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $this->addColumn('users', 'role', 'varchar(64) NOT NULL COLLATE utf8_unicode_ci');

       $this->addForeignKey(
         'user_fk_2',
         'users',
         'role',
         'auth_item',
         'name'
       );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropForeignKey('user_fk_2', 'users');

       $this->dropColumn('users', 'role');
    }
}
