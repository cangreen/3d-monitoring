<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%work_objects}}`.
 */
class m200323_131321_create_work_objects_table extends Migration
{

  private $objects1 = [
    [
      'name' => 'Чердак',
      'examination_type_id' => 1,
      'work_type_id' => 1,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Подвал',
      'examination_type_id' => 1,
      'work_type_id' => 1,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Мусорная камера',
      'examination_type_id' => 1,
      'work_type_id' => 1,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Отмостки',
      'examination_type_id' => 1,
      'work_type_id' => 1,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Приямки',
      'examination_type_id' => 1,
      'work_type_id' => 1,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Контейнерная площадка',
      'examination_type_id' => 1,
      'work_type_id' => 1,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Газоны',
      'examination_type_id' => 1,
      'work_type_id' => 1,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Придомовая территория',
      'examination_type_id' => 1,
      'work_type_id' => 1,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Примыкающая территория',
      'examination_type_id' => 1,
      'work_type_id' => 1,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Помещения',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Лестничная площадка',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Чердак',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Подвал',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Мусорная камера',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Мусороствол',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Контейнерные площадки',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Придомовая территория',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Газон',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Открытая веранда',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Лесопарк',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Прибрежная зона водоема',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Состояние водоема',
      'examination_type_id' => 1,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Помещение',
      'examination_type_id' => 1,
      'work_type_id' => 3,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Лестничная площадка',
      'examination_type_id' => 1,
      'work_type_id' => 3,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Контейнерные площадки',
      'examination_type_id' => 1,
      'work_type_id' => 4,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Детские площадки',
      'examination_type_id' => 1,
      'work_type_id' => 4,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Площадки для выгула собак',
      'examination_type_id' => 1,
      'work_type_id' => 4,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Помещение',
      'examination_type_id' => 1,
      'work_type_id' => 4,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Лестничная клетка',
      'examination_type_id' => 1,
      'work_type_id' => 4,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Чердак',
      'examination_type_id' => 1,
      'work_type_id' => 4,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Подвал',
      'examination_type_id' => 1,
      'work_type_id' => 4,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Мусорная камера',
      'examination_type_id' => 1,
      'work_type_id' => 4,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Вентиляция',
      'examination_type_id' => 1,
      'work_type_id' => 4,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Мусороствол',
      'examination_type_id' => 1,
      'work_type_id' => 4,
      'work_subtype_id' => 1
    ],

  ];

  private $objects2 = [
    [
      'name' => 'Чердак',
      'examination_type_id' => 2,
      'work_type_id' => 1,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Подвал',
      'examination_type_id' => 2,
      'work_type_id' => 1,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Мусорная камера',
      'examination_type_id' => 2,
      'work_type_id' => 1,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Приямки',
      'examination_type_id' => 2,
      'work_type_id' => 1,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Отмостки',
      'examination_type_id' => 2,
      'work_type_id' => 1,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Контейнерная площадка',
      'examination_type_id' => 2,
      'work_type_id' => 1,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Газон',
      'examination_type_id' => 2,
      'work_type_id' => 1,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Придомовая территория',
      'examination_type_id' => 2,
      'work_type_id' => 1,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Примыкающая территория',
      'examination_type_id' => 2,
      'work_type_id' => 1,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Контейнерная площадка',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Газон',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Лесопарк',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Прибрежная зона водоема',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Водное зеркало водоема',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Придомовая территория',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Открытая веранда',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 2
    ],
    [
      'name' => 'Помещения',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Лестничная площадка',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Чердак',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Подвал',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Мусорная камера',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
    [
      'name' => 'Мусороствол',
      'examination_type_id' => 2,
      'work_type_id' => 2,
      'work_subtype_id' => 1
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->createTable('{{%work_objects}}', [
      'id' => $this->primaryKey(),
      'name' => $this->string(32)->notNull(),
      'examination_type_id' => $this->integer()->notNull(),
      'work_type_id' => $this->integer()->notNull(),
      'work_subtype_id' => $this->integer()->notNull(),
    ]);

    $this->addForeignKey(
      'work_objects_fk1',
      'work_objects',
      'examination_type_id',
      'examination_types',
      'id'
    );

    $this->addForeignKey(
      'work_objects_fk2',
      'work_objects',
      'work_type_id',
      'work_types',
      'id'
    );
    $this->addForeignKey(
      'work_objects_fk3',
      'work_objects',
      'work_subtype_id',
      'work_subtypes',
      'id'
    );

    foreach ($this->objects1 as $value) {
      $this->insert('work_objects', [
        'name' => $value['name'],
        'examination_type_id' => $value['examination_type_id'],
        'work_type_id' => $value['work_type_id'],
        'work_subtype_id' =>$value['work_subtype_id'],
      ]);
    }

    foreach ($this->objects2 as $value) {
      $this->insert('work_objects', [
        'name' => $value['name'],
        'examination_type_id' => $value['examination_type_id'],
        'work_type_id' => $value['work_type_id'],
        'work_subtype_id' =>$value['work_subtype_id'],
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropForeignKey('work_objects_fk1', 'work_objects');
    $this->dropForeignKey('work_objects_fk2', 'work_objects');
    $this->dropForeignKey('work_objects_fk3', 'work_objects');
    $this->dropTable('{{%work_objects}}');
  }
}
