<?php

use yii\db\Migration;

/**
 * Class m200403_135650_alter_brigades_table
 */
class m200403_135650_alter_brigades_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('brigades', 'description');

        $this->addColumn('brigades', 'station_id', $this->integer(11)->notNull());

        $this->addForeignKey(
            'brigades_fk_1',
            'brigades',
            'station_id',
            'stations',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('brigades_fk_1','brigades');

        $this->dropColumn('brigades', 'station_id');

        $this->addColumn('brigades',  'description', $this->string(2048));
        return true;
    }
}
