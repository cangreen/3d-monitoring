<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%checklists}}`.
 */
class m200325_093015_create_checklists_table extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->createTable('{{%checklists}}', [
      'id' => $this->primaryKey(),
      'station_id' => $this->integer()->notNull(),
      'brigade_id' => $this->integer()->notNull(),
      'customer_id' => $this->integer()->notNull(),
      'examination_type_id' => $this->integer()->notNull(),
      'work_type_id' => $this->integer()->notNull(),
      'work_subtype_id' => $this->integer()->notNull(),
      'index' => $this->integer(6)->notNull(),
      'city' => $this->string(128)->notNull(),
      'district' => $this->string(128)->notNull(),
      'region' => $this->string(128)->notNull(),
      'street' => $this->string(128)->notNull(),
      'house' => $this->string(16)->notNull(),
      'flat' => $this->string(16)->notNull(),
      'lat' => $this->double()->notNull(),
      'lon' => $this->double()->notNull(),
      'address_full' => $this->string(1024)->notNull(),
      'created_at' => $this->integer(),
      'updated_at' => $this->integer()
    ]);

    $this->addForeignKey('checklist_fk_1', 'checklists', 'station_id', 'stations', 'id');
    $this->addForeignKey('checklist_fk_2', 'checklists', 'brigade_id', 'brigades', 'id');
    $this->addForeignKey('checklist_fk_3', 'checklists', 'customer_id', 'customers', 'id');
    $this->addForeignKey('checklist_fk_4', 'checklists', 'examination_type_id', 'examination_types', 'id');
    $this->addForeignKey('checklist_fk_5', 'checklists', 'work_type_id', 'work_types', 'id');
    $this->addForeignKey('checklist_fk_6', 'checklists', 'work_subtype_id', 'work_subtypes', 'id');
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropForeignKey('checklist_fk_1', 'checklists');
    $this->dropForeignKey('checklist_fk_2', 'checklists');
    $this->dropForeignKey('checklist_fk_3', 'checklists');
    $this->dropForeignKey('checklist_fk_4', 'checklists');
    $this->dropForeignKey('checklist_fk_5', 'checklists');
    $this->dropForeignKey('checklist_fk_6', 'checklists');

    $this->dropTable('{{%checklists}}');
  }
}
