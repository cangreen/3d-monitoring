<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%customers}}`.
 */
class m200323_090551_create_customers_table extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->createTable('{{%customers}}', [
      'id' => $this->primaryKey(),
      'category_id' => $this->integer()->notNull(),
      'name' => $this->string(256)->notNull(),
      'tax_number' => $this->string(12)->notNull(),
      'index' => $this->string(10)->notNull(),
      'city' => $this->string(128)->notNull(),
      'district' => $this->string(128)->notNull(),
      'region' => $this->string(128)->notNull(),
      'street' => $this->string(128)->notNull(),
      'house' => $this->string(16)->notNull(),
      'flat' => $this->string(16),
      'active' => $this->boolean()->notNull()->defaultValue(1),
      'created_at' => $this->integer(),
      'updated_at' => $this->integer()
    ]);

    $this->addForeignKey(
      'customers_fk1',
      'customers',
      'category_id',
      'customer_categories',
      'id',
      'CASCADE'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropForeignKey('customers_fk1', 'customers');

    $this->dropTable('{{%customers}}');
  }
}
