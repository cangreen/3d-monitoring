<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brigades}}`.
 */
class m200323_111228_create_brigades_table extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->createTable('{{%brigades}}', [
      'id' => $this->primaryKey(),
      'number' => $this->string(16)->notNull(),
      'description' => $this->string(2048),
      'created_at' => $this->integer(),
      'updated_at' => $this->integer()
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropTable('{{%brigades}}');
  }
}
