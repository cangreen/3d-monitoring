<?php

use yii\db\Migration;

/**
 * Class m200313_123324_alter_table_users_add_object_id
 */
class m200313_123324_alter_table_users_add_object_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->addColumn('users', 'station_id', $this->integer());

      $this->addForeignKey(
        'users_stations_fk',
        'users',
        'station_id',
        'stations',
        'id',
        'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropForeignKey('users_stations_fk', 'users');

      $this->dropColumn('users', 'stations_id');

    }

}
