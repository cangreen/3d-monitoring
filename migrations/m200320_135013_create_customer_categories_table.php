<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%customer_categories}}`.
 */
class m200320_135013_create_customer_categories_table extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->createTable('{{%customer_categories}}', [
      'id' => $this->primaryKey(),
      'name' => $this->string(128)->notNull(),
      'parent_id' => $this->integer()
    ]);

    $this->addForeignKey(
      'customer_categories_fk1',
      'customer_categories',
      'parent_id',
      'customer_categories',
      'id',
      'SET NULL'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropForeignKey(
      'customer_categories_fk1',
      'customer_categories');

    $this->dropTable('{{%customer_categories}}');
  }
}
