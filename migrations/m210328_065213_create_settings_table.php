<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m210328_065213_create_settings_table extends Migration
{
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->unique(),
            'alias' => $this->string(128),
            'value' => $this->string(128)
        ]);

        $this->insert('settings', [
            'name' => 'default_mark',
            'alias' => 'Оценка по умолчанию'
        ]);
    }

    public function down()
    {
        $this->dropTable('settings');
    }

}
