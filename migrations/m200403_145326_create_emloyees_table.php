<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%emloyees}}`.
 */
class m200403_145326_create_emloyees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employees}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'patronymic' => $this->string(64)->notNull(),
            'surname' => $this->string(64)->notNull(),
            'brigade_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'employees_fk_1',
            'employees',
            'brigade_id',
            'brigades',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('employees_fk_1', 'employees');

        $this->dropTable('{{%employees}}');
    }
}
