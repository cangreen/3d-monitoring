<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%examination_types}}`.
 */
class m200323_130034_create_examination_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%examination_types}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(16)->notNull()
        ]);

        $this->insert('examination_types', ['name' => 'СЭС']);

        $this->insert('examination_types', ['name' => 'СТС']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%examination_types}}');
    }
}
