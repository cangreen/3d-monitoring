<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%work_types}}`.
 */
class m200323_130459_create_work_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%work_types}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull()
        ]);

        $types = ['Дератизация', 'Дезинсекция', 'Дезинфекция очаговая', 'Дезинфекция профилактическая'];

        foreach ($types as $value) {
          $this->insert('work_types', ['name' => $value]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%work_types}}');
    }
}
