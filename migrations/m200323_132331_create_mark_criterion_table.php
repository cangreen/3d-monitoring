<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mark_criterion}}`.
 */
class m200323_132331_create_mark_criterion_table extends Migration
{
  private $criteria1 = [
    [
      'name' => 'Наличие нор грызунов',
      'work_object_id' => 1],
    [
      'name' => 'Погрызы грызунами',
      'work_object_id' => 1],
    [
      'name' => 'Наличие живых грызунов',
      'work_object_id' => 1],
    [
      'name' => 'Наличие павших грызунов',
      'work_object_id' => 1],
    [
      'name' => 'Поеда-емость приманки',
      'work_object_id' => 1],
    [
      'name' => 'Жалобы на наличие грызунов',
      'work_object_id' => 1],
    [
      'name' => 'Расставлены КПП',
      'work_object_id' => 1],
    [
      'name' => '% заселенности КПП',
      'work_object_id' => 1],
    [
      'name' => 'Расставле-ние КИК с приман-кой',
      'work_object_id' => 1],
    [
      'name' => '% выполнения предл. мероприятий',
      'work_object_id' => 1],
    [
      'name' => 'Регистрация укусов',
      'work_object_id' => 1],
    [
      'name' => 'Эффективность обработки',
      'work_object_id' => 1
    ],
    [
      'name' => 'Наличие нор грызунов',
      'work_object_id' => 2
    ],
    [
      'name' => 'Погрызы грызунами',
      'work_object_id' => 2
    ],
    [
      'name' => 'Наличие живых грызунов',
      'work_object_id' => 2
    ],
    [
      'name' => 'Наличие павших грызунов',
      'work_object_id' => 2
    ],
    [
      'name' => 'Поеда-емость приманки',
      'work_object_id' => 2
    ],
    [
      'name' => 'Жалобы на наличие грызунов',
      'work_object_id' => 2
    ],
    [
      'name' => 'Расставле-ние КПП',
      'work_object_id' => 2
    ],
    [
      'name' => '%% заселенности КПП',
      'work_object_id' => 2
    ],
    [
      'name' => 'Расставле-ние КИК с приман-кой',
      'work_object_id' => 2
    ],
    [
      'name' => '%% выполнения предл. мероприятий',
      'work_object_id' => 2
    ],
    [
      'name' => 'Регистрация укусов',
      'work_object_id' => 2
    ],
    [
      'name' => 'Эффективность обработки',
      'work_object_id' => 2
    ],

    [
      'name' => 'Наличие нор грызунов',
      'work_object_id' => 3
    ],
    [
      'name' => 'Погрызы грызунами',
      'work_object_id' => 3
    ],
    [
      'name' => 'Наличие живых грызунов',
      'work_object_id' => 3
    ],
    [
      'name' => 'Наличие павших грызунов',
      'work_object_id' => 3
    ],
    [
      'name' => 'Поеда-емость приманки',
      'work_object_id' => 3
    ],
    [
      'name' => 'Жалобы на наличие грызунов',
      'work_object_id' => 3
    ],
    [
      'name' => 'Расставле-ние КПП',
      'work_object_id' => 3
    ],
    [
      'name' => '%% заселенности КПП',
      'work_object_id' => 3
    ],
    [
      'name' => 'Расставле-ние КИК с приман-кой',
      'work_object_id' => 3
    ],
    [
      'name' => '%% выполнения предл. мероприятий',
      'work_object_id' => 3
    ],
    [
      'name' => 'Регистрация укусов',
      'work_object_id' => 3
    ],
    [
      'name' => 'Эффективность обработки',
      'work_object_id' => 3
    ],
    [
      'name' => 'Устранены разрушения',
      'work_object_id' => 4
    ],
    [
      'name' => 'Устранены затопления',
      'work_object_id' => 4
    ],
    [
      'name' => 'Устранены разрушения',
      'work_object_id' => 5
    ],
    [
      'name' => 'Устранены затопления',
      'work_object_id' => 5
    ],
    [
      'name' => 'Наличие нор грызунов',
      'work_object_id' => 6
    ],
    [
      'name' => 'Наличие живых грызунов',
      'work_object_id' => 6
    ],
    [
      'name' => 'Наличие павших грызунов',
      'work_object_id' => 6
    ],
    [
      'name' => 'Поеда-емость приманки',
      'work_object_id' => 6
    ],
    [
      'name' => 'Жалобы на наличие грызунов',
      'work_object_id' => 6
    ],
    [
      'name' => 'Обработка нор грызунов',
      'work_object_id' => 6
    ],
    [
      'name' => '%% выполнения предл. мероприятий',
      'work_object_id' => 6
    ],
    [
      'name' => 'Регистрация укусов',
      'work_object_id' => 6
    ],
    [
      'name' => 'Эффективность обработки',
      'work_object_id' => 6
    ],

    ['work_object_id' => 7, 'name' => 'Наличие нор грызунов'],
    ['work_object_id' => 7, 'name' => 'Погрызы грызунами'],
    ['work_object_id' => 7, 'name' => 'Наличие живых грызунов'],
    ['work_object_id' => 7, 'name' => 'Наличие павших грызунов'],
    ['work_object_id' => 7, 'name' => 'Поедаемость приманки'],
    ['work_object_id' => 7, 'name' => 'Жалобы на наличие грызунов'],
    ['work_object_id' => 7, 'name' => '%% выполнения предл. мероприятий'],
    ['work_object_id' => 7, 'name' => 'Регистрация укусов'],
    ['work_object_id' => 7, 'name' => 'Эффективность обработки'],
    ['work_object_id' => 8, 'name' => 'Наличие нор грызунов'],
    ['work_object_id' => 8, 'name' => 'Погрызы грызунами'],
    ['work_object_id' => 8, 'name' => 'Наличие живых грызунов'],
    ['work_object_id' => 8, 'name' => 'Наличие павших грызунов'],
    ['work_object_id' => 8, 'name' => 'Поедаемость приманки'],
    ['work_object_id' => 8, 'name' => 'Жалобы на наличие грызунов'],
    ['work_object_id' => 8, 'name' => '%% выполнения предл. мероприятий'],
    ['work_object_id' => 8, 'name' => 'Регистрация укусов'],
    ['work_object_id' => 8, 'name' => 'Эффективность обработки'],
    ['work_object_id' => 9, 'name' => 'Наличие нор грызунов'],
    ['work_object_id' => 9, 'name' => 'Погрызы грызунами'],
    ['work_object_id' => 9, 'name' => 'Наличие живых грызунов'],
    ['work_object_id' => 9, 'name' => 'Наличие павших грызунов'],
    ['work_object_id' => 9, 'name' => 'Поедаемость приманки'],
    ['work_object_id' => 9, 'name' => 'Жалобы на наличие грызунов'],
    ['work_object_id' => 9, 'name' => 'Регистрация укусов'],
    ['work_object_id' => 9, 'name' => 'Эффективность обработки'],
    ['work_object_id' => 10, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 10, 'name' => 'Наличие  животных'],
    ['work_object_id' => 10, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 10, 'name' => 'Эффективность обработки'],
    ['work_object_id' => 11, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 11, 'name' => 'Наличие  животных'],
    ['work_object_id' => 11, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 11, 'name' => 'Эффективность обработки'],
    ['work_object_id' => 12, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 12, 'name' => 'Наличие  животных'],
    ['work_object_id' => 12, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 12, 'name' => '%% выполнения предл. мероприятий'],
    ['work_object_id' => 12, 'name' => 'Эффективность обработки'],
    ['work_object_id' => 13, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 13, 'name' => 'Наличие  животных'],
    ['work_object_id' => 13, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 13, 'name' => '%% выполнения предл. мероприятий'],
    ['work_object_id' => 13, 'name' => 'Эффективность обработки'],
    ['work_object_id' => 14, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 14, 'name' => 'Наличие  животных'],
    ['work_object_id' => 14, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 14, 'name' => '%% выполнения предл. мероприятий'],
    ['work_object_id' => 14, 'name' => 'Эффективность обработки'],
    ['work_object_id' => 15, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 15, 'name' => '%% выполнения предл. мероприятий'],
    ['work_object_id' => 15, 'name' => 'Эффективность обработки'],
    ['work_object_id' => 16, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 16, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 16, 'name' => 'Эффектив-ность обработки'],
    ['work_object_id' => 16, 'name' => '% выполне-ния предл. мерооприятий'],
    ['work_object_id' => 17, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 17, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 17, 'name' => 'Эффективность обработки'],
    ['work_object_id' => 17, 'name' => '% выполнения предл. мерооприятий'],
    ['work_object_id' => 18, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 18, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 18, 'name' => 'Эффектив-ность обработки'],
    ['work_object_id' => 18, 'name' => '% выполне-ния предл. мероопри-ятий'],
    ['work_object_id' => 19, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 19, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 19, 'name' => 'Эффектив-ность обработки'],
    ['work_object_id' => 19, 'name' => '% выполне-ния предл. мерооприятий'],
    ['work_object_id' => 20, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 20, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 20, 'name' => 'Эффектив-ность обработки'],
    ['work_object_id' => 21, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 21, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 21, 'name' => 'Эффектив-ность обработки'],
    ['work_object_id' => 22, 'name' => 'Наличие членистоногих'],
    ['work_object_id' => 22, 'name' => 'Жалобы на наличие членистоногих'],
    ['work_object_id' => 22, 'name' => 'Эффектив-ность обработки'],
    ['work_object_id' => 23, 'name' => 'Качество соблюдения регламента обработки'],
    ['work_object_id' => 24, 'name' => 'Качество соблюдения регламента обработки'],
    ['work_object_id' => 25, 'name' => 'Качество соблюдения регламента обработки'],
    ['work_object_id' => 25, 'name' => 'Соблюдение регламента обработки'],
    ['work_object_id' => 26, 'name' => 'Качество соблюдения регламента обработки'],
    ['work_object_id' => 26, 'name' => 'Соблюдение регламента обработки'],
    ['work_object_id' => 27, 'name' => 'Качество соблюдения регламента обработки'],
    ['work_object_id' => 27, 'name' => 'Соблюдение регламента обработки'],
    ['work_object_id' => 28, 'name' => 'Качество соблюдения регламента обработки'],
    ['work_object_id' => 29, 'name' => 'Качество соблюдения регламента обработки'],
    ['work_object_id' => 30, 'name' => 'Качество соблюдения регламента обработки'],
    ['work_object_id' => 31, 'name' => 'Качество соблюдения регламента обработки'],
    ['work_object_id' => 32, 'name' => 'Качество соблюдения регламента обработки'],
    ['work_object_id' => 33, 'name' => 'Качество соблюдения регламента обработки'],
    ['work_object_id' => 34, 'name' => 'Качество соблюдения регламента обработки'],
  ];

  private $criteria2 = [
    ['work_object_id' => 35, 'name' => 'Проходимость'],
    ['work_object_id' => 35, 'name' => 'Освещение'],
    ['work_object_id' => 35, 'name' => 'Разрушения'],
    ['work_object_id' => 35, 'name' => 'Захламлен'],
    ['work_object_id' => 35, 'name' => 'Затопление'],
    ['work_object_id' => 36, 'name' => 'Проходимость'],
    ['work_object_id' => 36, 'name' => 'Освещение'],
    ['work_object_id' => 36, 'name' => 'Разрушение'],
    ['work_object_id' => 36, 'name' => 'Захламленность'],
    ['work_object_id' => 36, 'name' => 'Затопление'],
    ['work_object_id' => 36, 'name' => 'Вентпродух'],
    ['work_object_id' => 37, 'name' => 'Освещение'],
    ['work_object_id' => 37, 'name' => 'Разрушение'],
    ['work_object_id' => 37, 'name' => 'Захламленность'],
    ['work_object_id' => 37, 'name' => 'Качество уборки'],
    ['work_object_id' => 38, 'name' => 'Разрушения'],
    ['work_object_id' => 38, 'name' => 'Затопление'],
    ['work_object_id' => 39, 'name' => 'Разрушения'],
    ['work_object_id' => 39, 'name' => 'Затопление'],
    ['work_object_id' => 39, 'name' => 'Захламленность'],
    ['work_object_id' => 40, 'name' => 'Ограждение'],
    ['work_object_id' => 40, 'name' => 'Покрытие'],
    ['work_object_id' => 40, 'name' => 'Разрушение'],
    ['work_object_id' => 40, 'name' => 'Состояние контейнеров'],
    ['work_object_id' => 40, 'name' => 'Регулярность вывоза мусора'],
    ['work_object_id' => 41, 'name' => 'Благоустроен'],
    ['work_object_id' => 41, 'name' => 'Захламлен'],
    ['work_object_id' => 41, 'name' => 'Скошен-Нескошен'],
    ['work_object_id' => 41, 'name' => 'Резерв'],
    ['work_object_id' => 42, 'name' => 'Благоустроена'],
    ['work_object_id' => 42, 'name' => 'Захламлена'],
    ['work_object_id' => 43, 'name' => 'Благоустроена'],
    ['work_object_id' => 43, 'name' => 'Захламлена'],
    ['work_object_id' => 44, 'name' => 'Ограждение'],
    ['work_object_id' => 44, 'name' => 'Покрытие'],
    ['work_object_id' => 44, 'name' => 'Разрушение'],
    ['work_object_id' => 44, 'name' => 'Состояние контейнеров'],
    ['work_object_id' => 44, 'name' => 'Регулярность вывоза мусора'],
    ['work_object_id' => 45, 'name' => 'Благоустроен'],
    ['work_object_id' => 45, 'name' => 'Захламлен'],
    ['work_object_id' => 45, 'name' => 'Скошен/Нескошен'],
    ['work_object_id' => 46, 'name' => 'Благоустроен'],
    ['work_object_id' => 46, 'name' => 'Захламлен'],
    ['work_object_id' => 46, 'name' => 'Травяной покров скошен/нескошен'],
    ['work_object_id' => 47, 'name' => 'Бетонная отмостка (есть/нет)'],
    ['work_object_id' => 47, 'name' => 'Травяной покров скошен/нескошен'],
    ['work_object_id' => 47, 'name' => 'Заболоченность'],
    ['work_object_id' => 48, 'name' => 'Захламлено'],
    ['work_object_id' => 48, 'name' => 'Водная раститительность'],
    ['work_object_id' => 49, 'name' => 'Благоустроена'],
    ['work_object_id' => 49, 'name' => 'Захламлена'],
    ['work_object_id' => 50, 'name' => 'Благоустроена'],
    ['work_object_id' => 50, 'name' => 'Захламлена'],
    ['work_object_id' => 51, 'name' => 'Освещение'],
    ['work_object_id' => 51, 'name' => 'Разрушения'],
    ['work_object_id' => 51, 'name' => 'Захламленность'],
    ['work_object_id' => 51, 'name' => 'Качество уборки'],
    ['work_object_id' => 52, 'name' => 'Освещение'],
    ['work_object_id' => 52, 'name' => 'Захламленность'],
    ['work_object_id' => 52, 'name' => 'Качество уборки'],
    ['work_object_id' => 53, 'name' => 'Проходимость'],
    ['work_object_id' => 53, 'name' => 'Освещение'],
    ['work_object_id' => 53, 'name' => 'Разрушения'],
    ['work_object_id' => 53, 'name' => 'Захламленность'],
    ['work_object_id' => 53, 'name' => 'Затопление'],
    ['work_object_id' => 54, 'name' => 'Проходимость'],
    ['work_object_id' => 54, 'name' => 'Освещение'],
    ['work_object_id' => 54, 'name' => 'Разрушение'],
    ['work_object_id' => 54, 'name' => 'Захламленность'],
    ['work_object_id' => 54, 'name' => 'Затопление'],
    ['work_object_id' => 54, 'name' => 'Вентпродух'],
    ['work_object_id' => 55, 'name' => 'Освещение'],
    ['work_object_id' => 55, 'name' => 'Разрушение'],
    ['work_object_id' => 55, 'name' => 'Захламленность'],
    ['work_object_id' => 55, 'name' => 'Качество уборки'],
    ['work_object_id' => 56, 'name' => 'Герметичен'],
    ['work_object_id' => 56, 'name' => 'Захламлен'],

  ];

  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->createTable('{{%mark_criteria}}', [
      'id' => $this->primaryKey(),
      'name' => $this->string(64),
      'work_object_id' => $this->integer()
    ]);

    $this->addForeignKey(
      'mark_criteria_fk1',
      'mark_criteria',
      'work_object_id',
      'work_objects',
      'id'
    );

    foreach ($this->criteria1 as $value) {
      $this->insert('mark_criteria', [
        'name' => $value['name'],
        'work_object_id' => $value['work_object_id']
      ]);
    }

    foreach ($this->criteria2 as $value) {
      $this->insert('mark_criteria', [
        'name' => $value['name'],
        'work_object_id' => $value['work_object_id']
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropForeignKey('mark_criteria_fk1', 'mark_criteria');

    $this->dropTable('{{%mark_criteria}}');
  }
}
