<?php


namespace app\models;


use yii\base\Model;

class UserCreate extends Model
{
  public $email;
  public $password;
  public $station_id = false;
  public $name;
  public $surname;
  public $patronymic;
  public $phone;
  public $role;


  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['password', 'email', 'role'], 'required'],
      [['email', 'surname', 'patronymic'], 'string', 'max' => 128],
      ['email', 'unique', 'targetClass' => User::className()],
      ['password', 'string', 'min' => 6],
      ['email', 'email'],
      [['name', 'role'], 'string', 'max' => 64],
      [['phone'], 'string', 'max' => 15],
      [['station_id'], 'safe'],
      [['station_id'], 'default', 'value' => null],
      [['role'], 'string'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'password' => 'Пароль',
      'email' => 'Email',
      'name' => 'Имя',
      'surname' => 'Фамилия',
      'patronymic' => 'Отчество',
      'phone' => 'Телефон',
      'station_id' => 'Объект',
      'stationName' => 'Объект',
      'role' => 'Роль',
    ];
  }

  public function create()
  {
    if (!$this->validate()) {
      return false;
    }
    $user = new User();
    $user->email = $this->email;
    if ($this->station_id) {
      $user->station_id = $this->station_id;
    }
    $user->name = $this->name;
    $user->surname = $this->surname;
    $user->patronymic = $this->patronymic;
    $user->phone = $this->phone;
    $user->role = $this->role;

    $user->setPassword($this->password);
    $user->generateAuthKey();
    return $user->save() && $this->setUserRole($user->id);
  }

  private function setUserRole($id)
  {
    $am = \Yii::$app->authManager;
    $role = $am->getRole($this->role);
    if (!$role) return false;
    $am->assign($role, $id);
    return true;
  }
}