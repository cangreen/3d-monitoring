<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * MarksSearch represents the model behind the search form of `app\models\Marks`.
 */
class MarksSearch extends Marks
{
    public $checklistDate;
    public $criterionName;
    public $dateFrom;
    public $dateTo;
    public $nullMarks;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'checklist_id', 'criterion_id', 'value'], 'integer'],
            [['checklistDate', 'criterionName', 'nullMarks'], 'safe'],
            [['dateFrom', 'dateTo'], 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Marks::find();
        $user = Yii::$app->user;

        if (!$user->can('superadmin')) {
            $query->andWhere(['checklists.station_id' => $user->identity->station_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'checklist_id' => [
                    'asc' => ['checklist_id' => SORT_DESC],
                    'desc' => ['checklist_id' => SORT_ASC]
                ],
                'checklistDate' => [
                    'asc' => ['checklists.created_at' => SORT_ASC],
                    'desc' => ['checklists.created_at' => SORT_DESC]
                ],
                'criterionName' => [
                    'asc' => ['mark_criteria.name' => SORT_ASC],
                    'desc' => ['mark_criteria.name' => SORT_DESC]
                ],
                'value' => [
                    'asc' => ['value' => SORT_ASC],
                    'desc' => ['value' => SORT_DESC]
                ],
            ],
        ]);

        $this->load($params);

        if ($this->nullMarks === '0') {
            $query->andFilterWhere(['<>', 'value', 0]);
        }

        if (!empty($this->checklistDate)) {
            $this->filterByDate($query);
        } else {
            $query->joinWith(['checklist' => function ($q) {
                $q->andFilterWhere(['checklists.created_at' => $this->checklistDate]);
            }]);
        }

        if (!$this->validate()) {
            $query->joinWith(['checklist']);
            $query->joinWith(['criterion']);
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'checklist_id' => $this->checklist_id,
            'criterion_id' => $this->criterion_id,
            'value' => $this->value,
        ]);

        $query->andFilterWhere(['like', 'mark_criteria.name', $this->criterionName]);

        $query->joinWith(['criterion' => function ($q) {
            $q->where('mark_criteria.name LIKE "%' . $this->criterionName . '%"');
        }]);

        if (!empty($this->dateFrom) && !empty($this->dateTo)) {
            $query->andFilterWhere([
                'between',
                'checklists.created_at',
                (int)\Yii::$app->formatter->asTimestamp($this->dateFrom . ' 00:00'),
                (int)\Yii::$app->formatter->asTimestamp($this->dateTo . ' 23:59'),
            ]);
        }


        return $dataProvider;
    }

    private function filterByDate(ActiveQuery $query)
    {
        $query->joinWith(['checklist' => function (ActiveQuery $q) {

            $dayStart = (int)\Yii::$app->formatter->asTimestamp($this->checklistDate . ' 00:00:00');
            $dayStop = (int)\Yii::$app->formatter->asTimestamp($this->checklistDate . ' 23:59:59');
            $q->andFilterWhere([
                'between',
                'checklists.created_at',
                $dayStart,
                $dayStop,
            ]);
        }]);
    }

    public function attributeLabels()
    {
        return [
            'dateFrom' => 'Начало периода',
            'dateTo' => 'Окончание периода',
            'nullMarks' => 'Включать оценки 0'
        ];
    }
}
