<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mark_criteria".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $work_object_id
 *
 * @property WorkObjects $workObject
 * @property ExaminationTypes $examinationType
 * @property string $workObjectName
 * @property string $examinationTypeName
 * @property WorkTypes $workType
 * @property string $workTypeName
 * @property WorkSubTypes $workSubType
 * @property string $workSubTypeName
 */
class MarkCriteria extends \yii\db\ActiveRecord
{
  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'mark_criteria';
  }

  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['work_object_id'], 'integer'],
      [['name'], 'string', 'max' => 64],
      [['work_object_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkObjects::className(), 'targetAttribute' => ['work_object_id' => 'id']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'name' => 'Название',
      'work_object_id' => 'Объект обработки',
      'workObjectName' => 'Объект обработки',
      'examinationTypeName' => 'Вид обследования',
      'workTypeName' => 'Вид работ',
      'workSubTypeName' => 'Подвид работ',
    ];
  }

  /**
   * Gets query for [[WorkObject]].
   *
   * @return \yii\db\ActiveQuery
   */
  public function getWorkObject()
  {
    return $this->hasOne(WorkObjects::className(), ['id' => 'work_object_id']);
  }

  /**
   * @return ExaminationTypes
   */
  public function getExaminationType()
  {
    return $this->workObject->examinationType;
  }

  /**
   * @return string
   */
  public function getExaminationTypeName()
  {
    return $this->examinationType->name;
  }

  /**
   * @return WorkTypes
   */
  public function getWorkType()
  {
    return $this->workObject->workType;
  }

  /**
   * @return string
   */
  public function getWorkTypeName()
  {
    return $this->workType->name;
  }

  /**
   * @return WorkSubtypes
   */
  public function getWorkSubType()
  {
    return $this->workObject->workSubtype;
  }

  /**
   * @return string
   */
  public function getWorkSubTypeName()
  {
    return $this->workSubType->name;
  }


  /**
   * @return string
   */
  public function getWorkObjectName()
  {
    return $this->workObject->name;
  }
}
