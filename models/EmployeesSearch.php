<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Employees;
use yii\db\ActiveQuery;
use function foo\func;

/**
 * EmployeesSearch represents the model behind the search form of `app\models\Employees`.
 */
class EmployeesSearch extends Employees
{
    public $brigadeNumber;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'brigade_id'], 'integer'],
            [['name', 'patronymic', 'surname', 'brigadeNumber'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employees::find();

        $user = Yii::$app->user;

        if(!$user->can('superadmin')) {
            $query->where(['brigades.station_id' => $user->identity->station_id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_DESC],
                    'desc' => ['id' => SORT_ASC]
                ],
                'name' => [
                    'asc' => ['name' => SORT_ASC],
                    'desc' => ['name' => SORT_DESC]
                ],
                'patronymic' => [
                    'asc' => ['patronymic' => SORT_ASC],
                    'desc' => ['patronymic' => SORT_DESC]
                ],
                'surname' => [
                    'asc' => ['surname' => SORT_ASC],
                    'desc' => ['surname' => SORT_DESC]
                ],
                'brigadeNumber' => [
                    'asc' => ['brigades.number' => SORT_ASC],
                    'desc' => ['brigades.number' => SORT_DESC]
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['brigade']);
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'brigade_id' => $this->brigade_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'patronymic', $this->patronymic])
            ->andFilterWhere(['like', 'surname', $this->surname]);

        $query->joinWith(['brigade' => function ($q) {
            $q->where('brigades.number LIKE "%' . $this->brigadeNumber . '%"');
        }]);

        return $dataProvider;
    }
}
