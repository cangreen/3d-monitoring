<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "brigades".
 *
 * @property int $id
 * @property string $number
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int $station_id
 *
 * @property Stations $station
 * @property string $stationName
 * @property Checklists[] $checklists
 */
class Brigades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brigades';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'station_id'], 'required'],
            [['created_at', 'updated_at', 'station_id'], 'integer'],
            [['number'], 'string', 'max' => 16],
            [['station_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Stations::className(), 'targetAttribute' => ['station_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'description' => 'Описание',
            'station_id' => 'Дез. станция',
            'stationName' => 'Дез. станция',
            'created_at' => 'Дата/время создания',
            'updated_at' => 'Дата/время изменения',
        ];
    }

    /**
     * Gets query for [[Station]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Stations::className(), ['id' => 'station_id']);
    }

    public function getStationName()
    {
        return $this->station->name;
    }

    /**
     * Gets query for [[Checklists]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChecklists()
    {
        return $this->hasMany(Checklists::className(), ['brigade_id' => 'id']);
    }
}
