<?php

namespace app\models;

use phpDocumentor\Reflection\Types\This;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use function MongoDB\BSON\toJSON;

/**
 * This is the model class for table "checklists".
 *
 * @property int $id
 * @property int $station_id
 * @property int $brigade_id
 * @property int $customer_id
 * @property int $examination_type_id
 * @property int $work_type_id
 * @property int $work_subtype_id
 * @property int $index
 * @property string $city
 * @property string $district
 * @property string $region
 * @property string $street
 * @property string $house
 * @property string|null $flat
 * @property float|null $lat
 * @property float|null $lon
 * @property string $address_full
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Stations $station
 * @property string $stationName
 * @property Brigades $brigade
 * @property string $brigadeNumber
 * @property Customers $customer
 * @property string $customerName
 * @property ExaminationTypes $examinationType
 * @property string $examinationTypeName
 * @property WorkTypes $workType
 * @property string $workTypeName
 * @property WorkSubtypes $workSubtype
 * @property string $workSubtypeName
 * @property string $address
 */
class Checklists extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'checklists';
    }


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className()
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['station_id', 'brigade_id', 'customer_id', 'examination_type_id', 'work_type_id', 'work_subtype_id', 'index',
                'city', 'district', 'region', 'street', 'house', 'address_full', 'lat', 'lon'], 'required'],
            [['station_id', 'brigade_id', 'customer_id', 'examination_type_id', 'work_type_id', 'work_subtype_id', 'index', 'created_at', 'updated_at'], 'integer'],
            [['lat', 'lon'], 'number'],
            [['city', 'district', 'region', 'street'], 'string', 'max' => 128],
            [['house', 'flat'], 'string', 'max' => 16],
            [['address_full'], 'string', 'max' => 1024],
            [['station_id'], 'exist', 'skipOnError' => true, 'targetClass' => Stations::className(), 'targetAttribute' => ['station_id' => 'id']],
            [['brigade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brigades::className(), 'targetAttribute' => ['brigade_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['examination_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExaminationTypes::className(), 'targetAttribute' => ['examination_type_id' => 'id']],
            [['work_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkTypes::className(), 'targetAttribute' => ['work_type_id' => 'id']],
            [['work_subtype_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkSubtypes::className(), 'targetAttribute' => ['work_subtype_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'station_id' => 'Дезстанция',
            'stationName' => 'Дезстанция',
            'brigade_id' => 'Бригада',
            'brigadeNumber' => 'Бригада',
            'customer_id' => 'Заказчик',
            'customerName' => 'Заказчик',
            'examination_type_id' => 'Вид обследования',
            'examinationTypeName' => 'Вид обследования',
            'work_type_id' => 'Вид работ',
            'workTypeName' => 'Вид работ',
            'work_subtype_id' => 'Подвид работ',
            'workSubTypeName' => 'Подвид работ',
            'index' => 'Индекс',
            'city' => 'Город',
            'district' => 'Округ',
            'region' => 'Район',
            'street' => 'Улица',
            'address' => 'Адрес',
            'house' => 'Дом',
            'flat' => 'Квартира/офис',
            'lat' => 'Широта',
            'lon' => 'Долгота',
            'address_full' => 'Полный адрес',
            'created_at' => 'Дата',
        ];
    }

    /**
     * Gets query for [[Station]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Stations::className(), ['id' => 'station_id']);
    }

    /**
     * @return string
     */
    public function getStationName()
    {
        return $this->station->name;
    }

    /**
     * Gets query for [[Brigade]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrigade()
    {
        return $this->hasOne(Brigades::className(), ['id' => 'brigade_id']);
    }

    /**
     * @return string
     */
    public function getBrigadeNumber()
    {
        return $this->brigade->number;
    }

    /**
     * Gets query for [[Customer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customer->name;
    }

    /**
     * Gets query for [[ExaminationType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExaminationType()
    {
        return $this->hasOne(ExaminationTypes::className(), ['id' => 'examination_type_id']);
    }

    /**
     * @return string
     */
    public function getExaminationTypeName()
    {
        return $this->examinationType->name;
    }

    /**
     * Gets query for [[WorkType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkType()
    {
        return $this->hasOne(WorkTypes::className(), ['id' => 'work_type_id']);
    }

    /**
     * @return string
     */
    public function getWorkTypeName()
    {
        return $this->workType->name;
    }


    /**
     * Gets query for [[WorkSubtype]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkSubtype()
    {
        return $this->hasOne(WorkSubtypes::className(), ['id' => 'work_subtype_id']);
    }

    /**
     * @return string
     */
    public function getWorkSubTypeName()
    {
        return $this->workSubtype->name;
    }

    /**
     * @param array $marks
     * @param null $checklist_id
     * @return bool
     * @throws \yii\db\Exception
     */

    public function saveMarks(array $marks, $checklist_id = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        if (!empty($marks)) {
            if (!is_null($checklist_id) && Marks::deleteAll(['checklist_id' => $checklist_id]) === 0) {
                $transaction->rollBack();
                return false;
            }
            foreach ($marks as $id => $value) {
                $model = new Marks();
                $model->checklist_id = $this->id;
                $model->criterion_id = $id;
                $model->value = $value;
                if (!$model->save()) {
                    var_dump($model->errors);
                    $transaction->rollBack();
                    return false;
                }
            }
        }
        $transaction->commit();
        return true;
    }


    /**
     * Возвращает массив критериев оценки для существующего чеклиста, включая уже выставленные оценки (для checklist/update)
     * @return array
     */
    public function getMarkCriteriaWithMarks()
    {
        $result = [];

        $objects = WorkObjects::find()->where([
            'examination_type_id' => $this->examination_type_id,
            'work_type_id' => $this->work_type_id,
            'work_subtype_id' => $this->work_subtype_id
        ])->all();

        foreach ($objects as $object) {
            $result[$object->id] = [
                'name' => $object->name,
                'criteria' => ArrayHelper::toArray($object->getMarkCriterias()->all(), [
                    MarkCriteria::class => [
                        'id',
                        'name',
                        'mark' => function (MarkCriteria $model) {
                            $value = Marks::find()->select(['value'])->where([
                                'checklist_id' => $this->id,
                                'criterion_id' => $model->id
                            ])->all();
                            if (!empty($value)) {
                                return $value[0]['value'];
                            }
                        }
                    ]
                ])
            ];
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return "{$this->street}, {$this->house}";
    }
}
