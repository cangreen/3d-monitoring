<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WorkObjects;

/**
 * WorkObjectsSearch represents the model behind the search form of `app\models\WorkObjects`.
 */
class WorkObjectsSearch extends WorkObjects
{
  public $examinationTypeName;
  public $workTypeName;
  public $workSubtypeName;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'examination_type_id', 'work_type_id', 'work_subtype_id'], 'integer'],
      [['name', 'examinationTypeName', 'workTypeName', 'workSubtypeName'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $query = WorkObjects::find();

    // add conditions that should always apply here

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
    ]);

    $dataProvider->setSort([
      'attributes' => [
        'id' => [
          'asc' => ['id' => SORT_DESC],
          'desc' => ['id' => SORT_ASC]
        ],
        'examination_type_id' => [
          'asc' => ['examination_type_id' => SORT_ASC],
          'desc' => ['examination_type_id' => SORT_DESC]
        ],
        'work_type_id' => [
          'asc' => ['work_type_id' => SORT_ASC],
          'desc' => ['work_type_id' => SORT_DESC]
        ],
        'work_subtype_id' => [
          'asc' => ['work_subtype_id' => SORT_ASC],
          'desc' => ['work_subtype_id' => SORT_DESC]
        ],
        'name' => [
          'asc' => ['name' => SORT_ASC],
          'desc' => ['name' => SORT_DESC]
        ],
        'tax_number' => [
          'asc' => ['tax_number' => SORT_ASC],
          'desc' => ['tax_number' => SORT_DESC]
        ],
        'active' => [
          'asc' => ['active' => SORT_ASC],
          'desc' => ['active' => SORT_DESC]
        ],
      ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      $query->joinWith(['examinationType']);
      $query->joinWith(['workType']);
      $query->joinWith(['workSubtype']);
      return $dataProvider;
    }

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'examination_type_id' => $this->examination_type_id,
      'work_type_id' => $this->work_type_id,
      'work_subtype_id' => $this->work_subtype_id,
    ]);

    $query->andFilterWhere(['like', 'work_objects.name', $this->name]);

    $query->joinWith(['examinationType' => function ($q) {
      $q->where('examination_types.name LIKE "%' . $this->examinationTypeName . '%"');
    }]);

    $query->joinWith(['workType' => function ($q) {
      $q->where('work_types.name LIKE "%' . $this->workTypeName . '%"');
    }]);

    $query->joinWith(['workSubtype' => function ($q) {
      $q->where('work_subtypes.name LIKE "%' . $this->workSubtypeName . '%"');
    }]);

    return $dataProvider;
  }

}
