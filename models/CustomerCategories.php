<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_categories".
 *
 * @property int $id
 * @property string $name
 * @property int|null $parent_id
 *
 * @property CustomerCategories $parent
 * @property CustomerCategories[] $customerCategories
 */
class CustomerCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parent_id'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerCategories::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'parent_id' => 'Родительская категория',
            'parentName' => 'Родительская категория',
        ];
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(CustomerCategories::className(), ['id' => 'parent_id']);
    }

  /**
   * @return string
   */
    public function getParentName()
    {
        return $this->parent->name;
    }

    /**
     * Gets query for [[CustomerCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerCategories()
    {
        return $this->hasMany(CustomerCategories::className(), ['parent_id' => 'id']);
    }
}
