<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "examination_types".
 *
 * @property int $id
 * @property string $name
 *
 * @property WorkObjects[] $workObjects
 */
class ExaminationTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'examination_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * Gets query for [[WorkObjects]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkObjects()
    {
        return $this->hasMany(WorkObjects::className(), ['examination_type_id' => 'id']);
    }
}
