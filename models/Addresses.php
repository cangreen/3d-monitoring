<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "addresses".
 *
 * @property string|null $district
 * @property string|null $region
 * @property string|null $street
 * @property int|null $building
 */
class Addresses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addresses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building'], 'integer'],
            [['district', 'region', 'street'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'district' => 'District',
            'region' => 'Region',
            'street' => 'Street',
            'building' => 'Building',
        ];
    }
}
