<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customers".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $tax_number
 * @property string $index
 * @property string $city
 * @property string $district
 * @property string $region
 * @property string $street
 * @property string $house
 * @property string|null $flat
 * @property int $active
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property CustomerCategories $category
 */
class Customers extends \yii\db\ActiveRecord
{
  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'customers';
  }

  public function behaviors()
  {
    return [
      TimestampBehavior::className(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['category_id', 'name', 'tax_number', 'index', 'city', 'district', 'region', 'street', 'house'], 'required'],
      [['category_id', 'active', 'created_at', 'updated_at'], 'integer'],
      [['name'], 'string', 'max' => 256],
      [['tax_number'], 'string', 'max' => 12],
      [['index'], 'string', 'max' => 10],
      [['city', 'district', 'region', 'street'], 'string', 'max' => 128],
      [['house'], 'string', 'max' => 16],
      [['flat'], 'string', 'max' => 16],
      [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerCategories::className(), 'targetAttribute' => ['category_id' => 'id']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'category_id' => 'Категория',
      'categoryName' => 'Категория',
      'name' => 'Название',
      'tax_number' => 'ИНН',
      'index' => 'Индекс',
      'city' => 'Город',
      'district' => 'Округ',
      'region' => 'Район',
      'street' => 'Улица',
      'house' => 'Дом',
      'flat' => 'квартира/офис',
      'active' => 'Активна',
      'created_at' => 'Создана',
      'updated_at' => 'Изменена',
    ];
  }

  /**
   * Gets query for [[Category]].
   *
   * @return \yii\db\ActiveQuery
   */
  public function getCategory()
  {
    return $this->hasOne(CustomerCategories::className(), ['id' => 'category_id']);
  }

  public function getCategoryName()
  {
    return $this->category->name;
  }
}
