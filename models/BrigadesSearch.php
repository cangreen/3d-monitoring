<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Brigades;

/**
 * BrigadesSearch represents the model behind the search form of `app\models\Brigades`.
 */
class BrigadesSearch extends Brigades
{
    public $stationName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'station_id', 'created_at', 'updated_at'], 'integer'],
            [['number', 'stationName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Brigades::find();

        $user = Yii::$app->user;

        if(!$user->can('superadmin')) {
            $query->andWhere(['station_id' => $user->identity->station_id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_DESC],
                    'desc' => ['id' => SORT_ASC]
                ],
                'number' => [
                    'asc' => ['number' => SORT_ASC],
                    'desc' => ['number' => SORT_DESC]
                ],
                'stationName' => [
                    'asc' => ['stations.name' => SORT_ASC],
                    'desc' => ['stations.name' => SORT_DESC]
                ],
            ],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            $query->joinWith(['station']);
            return $dataProvider;
        }

        $query->joinWith(['station' => function ($q) {
            $q->where('stations.name LIKE "%' . $this->stationName . '%"');
        }]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'station_id' => $this->station_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
