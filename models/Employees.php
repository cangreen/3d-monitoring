<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employees".
 *
 * @property int $id
 * @property string $name
 * @property string $patronymic
 * @property string $surname
 * @property int $brigade_id
 *
 * @property Brigades $brigade
 * @property string $brigadeNumber
 */
class Employees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'patronymic', 'surname', 'brigade_id'], 'required'],
            [['brigade_id'], 'integer'],
            [['name', 'patronymic', 'surname'], 'string', 'max' => 64],
            [['brigade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brigades::className(), 'targetAttribute' => ['brigade_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'patronymic' => 'Отчество',
            'surname' => 'Фамилия',
            'brigade_id' => 'Бригада',
            'brigadeNumber' => 'Бригада',
        ];
    }

    /**
     * Gets query for [[Brigade]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrigade()
    {
        return $this->hasOne(Brigades::className(), ['id' => 'brigade_id']);
    }

    /**
     * @return string
     */
    public function getBrigadeNumber()
    {
        return $this->brigade->number;
    }
}
