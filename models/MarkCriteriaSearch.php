<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MarkCriteria;
use yii\db\ActiveQuery;

/**
 * MarkCriteriaSearch represents the model behind the search form of `app\models\MarkCriteria`.
 */
class MarkCriteriaSearch extends MarkCriteria
{
    public $workObjectName;
    public $examinationTypeName;
    public $workTypeName;
    public $workSubTypeName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'work_object_id'], 'integer'],
            [['name', 'workObjectName', 'examinationTypeName', 'workTypeName', 'workSubTypeName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MarkCriteria::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_DESC],
                    'desc' => ['id' => SORT_ASC]
                ],
                'name' => [
                    'asc' => ['name' => SORT_DESC],
                    'desc' => ['name' => SORT_ASC]
                ],
                'workObjectName' => [
                    'asc' => ['work_objects.name' => SORT_ASC],
                    'desc' => ['work_objects.name' => SORT_DESC]
                ],
                'examinationTypeName' => [
                    'asc' => ['work_objects.examination_type_id' => SORT_ASC],
                    'desc' => ['work_objects.examination_type_id' => SORT_DESC]
                ],
                'workTypeName' => [
                    'asc' => ['work_objects.work_type_id' => SORT_ASC],
                    'desc' => ['work_objects.work_type_id' => SORT_DESC]
                ],
                'workSubTypeName' => [
                    'asc' => ['work_objects.work_subtype_id' => SORT_ASC],
                    'desc' => ['work_objects.work_subtype_id' => SORT_DESC]
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->joinWith(['workObject']);
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'work_object_id' => $this->work_object_id,
        ]);

        $query->andFilterWhere(['like', 'mark_criteria.name', $this->name]);

        $query->joinWith(['workObject' => function (ActiveQuery $q) {
            $q->where('work_objects.name LIKE "%' . $this->workObjectName . '%"')
                ->joinWith([
                    'examinationType' => function (ActiveQuery $q) {
                        $q->where('examination_types.name LIKE "%' . $this->examinationTypeName . '%"');
                    },
                    'workType' => function (ActiveQuery $q) {
                        $q->where('work_types.name LIKE "%' . $this->workTypeName . '%"');
                    },
                    'workSubtype' => function (ActiveQuery $q) {
                        $q->where('work_subtypes.name LIKE "%' . $this->workSubTypeName . '%"');
                    }
                ]);
        }]);

        return $dataProvider;
    }
}
