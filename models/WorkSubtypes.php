<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_subtypes".
 *
 * @property int $id
 * @property string $name
 *
 * @property WorkObjects[] $workObjects
 */
class WorkSubtypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_subtypes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * Gets query for [[WorkObjects]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkObjects()
    {
        return $this->hasMany(WorkObjects::className(), ['work_subtype_id' => 'id']);
    }
}
