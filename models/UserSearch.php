<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
use yii\db\Expression;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'active', 'created_at', 'updated_at'], 'integer'],
      [['email', 'station_id', 'password_hash', 'auth_key', 'password_reset_token', 'name', 'surname', 'patronymic', 'phone', 'role'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $query = User::find();

    // add conditions that should always apply here

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
       $query->where('0=1');
      return $dataProvider;
    }

    // grid filtering conditions

    if ($this->station_id === '0') {
      $query->andFilterWhere(['IS', 'station_id', new Expression('null')]);
    }
    else {
      $query->andFilterWhere(['station_id' => $this->station_id]);
    }
    
    $query->andFilterWhere([
      'id' => $this->id,
      'active' => $this->active,
      'created_at' => $this->created_at,
      'updated_at' => $this->updated_at,
    ]);


    $query->andFilterWhere(['like', 'email', $this->email])
      ->andFilterWhere(['like', 'password_hash', $this->password_hash])
      ->andFilterWhere(['like', 'auth_key', $this->auth_key])
      ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
      ->andFilterWhere(['like', 'name', $this->name])
      ->andFilterWhere(['like', 'surname', $this->surname])
      ->andFilterWhere(['like', 'patronymic', $this->patronymic])
      ->andFilterWhere(['like', 'phone', $this->phone])
      ->andFilterWhere(['like', 'role', $this->role]);

    return $dataProvider;
  }
}
