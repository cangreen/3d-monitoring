<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "stations".
 *
 * @property int $id
 * @property int $number
 * @property string $name
 * @property string $director_name
 * @property string $director_surname
 * @property string $director_patronymic
 * @property string $phone
 * @property string $email
 * @property string $index
 * @property string $city
 * @property string $district
 * @property string $region
 * @property string $street
 * @property string $house
 * @property string|null $flat
 * @property int $active
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User[] $user
 */
class Stations extends \yii\db\ActiveRecord
{

  public function behaviors()
  {
    return [
      TimestampBehavior::className(),
    ];
  }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'name', 'director_name', 'director_surname', 'director_patronymic', 'phone', 'email',
              'index', 'city', 'district', 'region', 'street', 'house'], 'required'],
            [['number', 'active', 'created_at', 'updated_at'], 'integer'],
            [['name', 'director_name', 'director_surname', 'director_patronymic', 'email', 'city', 'district', 'region'], 'string', 'max' => 128],
            [['phone'], 'string', 'max' => 15],
            [['index'], 'string', 'max' => 10],
            [['house', 'flat'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'name' => 'Название',
            'director_name' => 'Имя директора',
            'director_surname' => 'Фамилия директора',
            'director_patronymic' => 'Отчество директора',
            'phone' => 'Телефон',
            'email' => 'Email',
            'index' => 'Индекс',
            'city' => 'Город',
            'district' => 'Округ',
            'region' => 'Район',
            'street' => 'Улица',
            'house' => 'Дом',
            'flat' => 'Офис (квартира)',
            'active' => 'Активна',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлён',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasMany(User::className(), ['station' => 'id']);
    }


    /**
     * Gets query for [[Brigades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrigades()
    {
        return $this->hasMany(Brigades::className(), ['station_id' => 'id']);
    }

}
