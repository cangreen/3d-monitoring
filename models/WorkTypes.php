<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_types".
 *
 * @property int $id
 * @property string $name
 *
 * @property WorkObjects[] $workObjects
 */
class WorkTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * Gets query for [[WorkObjects]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkObjects()
    {
        return $this->hasMany(WorkObjects::className(), ['work_type_id' => 'id']);
    }
}
