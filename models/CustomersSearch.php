<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customers;

/**
 * CustomersSearch represents the model behind the search form of `app\models\Customers`.
 */
class CustomersSearch extends Customers
{
  public $categoryName;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'category_id', 'active', 'created_at', 'updated_at'], 'integer'],
      [['name', 'tax_number', 'index', 'city', 'district', 'region', 'street', 'house', 'flat', 'categoryName'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $query = Customers::find();

    // add conditions that should always apply here

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
    ]);

    $dataProvider->setSort([
      'attributes' => [
        'id' => [
          'asc' => ['id' => SORT_DESC],
          'desc' => ['id' => SORT_ASC]
        ],
        'categoryName' => [
          'asc' => ['customer_categories.name' => SORT_ASC],
          'desc' => ['customer_categories.name' => SORT_DESC]
        ],
        'name' => [
          'asc' => ['name' => SORT_ASC],
          'desc' => ['name' => SORT_DESC]
        ],
        'tax_number' => [
          'asc' => ['tax_number' => SORT_ASC],
          'desc' => ['tax_number' => SORT_DESC]
        ],
        'active' => [
          'asc' => ['active' => SORT_ASC],
          'desc' => ['active' => SORT_DESC]
        ],
      ],
    ]);


    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      $query->joinWith(['category']);
      return $dataProvider;
    }

    $this->addCondition($query, 'category_id');

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'category_id' => $this->category_id,
      'active' => $this->active,
      'created_at' => $this->created_at,
      'updated_at' => $this->updated_at,
    ]);

    $query->andFilterWhere(['like', 'name', $this->name])
      ->andFilterWhere(['like', 'tax_number', $this->tax_number])
      ->andFilterWhere(['like', 'index', $this->index])
      ->andFilterWhere(['like', 'city', $this->city])
      ->andFilterWhere(['like', 'district', $this->district])
      ->andFilterWhere(['like', 'region', $this->region])
      ->andFilterWhere(['like', 'street', $this->street])
      ->andFilterWhere(['like', 'house', $this->house])
      ->andFilterWhere(['like', 'flat', $this->flat]);

    $query->joinWith(['category' => function ($q) {
      $q->where('customer_categories.name LIKE "%' . $this->categoryName . '%"');
    }]);

    return $dataProvider;
  }

  protected function addCondition($query, $attribute, $partialMatch = false)
  {
    if (($pos = strrpos($attribute, '.')) !== false) {
      $modelAttribute = substr($attribute, $pos + 1);
    } else {
      $modelAttribute = $attribute;
    }
    $value = $this->$modelAttribute;
    if (trim($value) === '') {
      return;
    }
    if ($partialMatch) {
      $query->andWhere(['like', $attribute, $value]);
    } else {
      $query->andWhere([$attribute => $value]);
    }
  }

}
