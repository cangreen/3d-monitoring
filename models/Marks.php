<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marks".
 *
 * @property int $id
 * @property int $checklist_id
 * @property int $criterion_id
 * @property string $criterionName
 * @property int $value
 * @property int $checklistDate
 *
 * @property Checklists $checklist
 * @property MarkCriteria $criterion
 */
class Marks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['checklist_id', 'criterion_id', 'value'], 'required'],
            [['checklist_id', 'criterion_id', 'value'], 'integer'],
            ['value', 'in', 'range' => [0, 1, 2, 3, 4]],
            [['checklist_id'], 'exist', 'skipOnError' => true, 'targetClass' => Checklists::className(), 'targetAttribute' => ['checklist_id' => 'id']],
            [['criterion_id'], 'exist', 'skipOnError' => true, 'targetClass' => MarkCriteria::className(), 'targetAttribute' => ['criterion_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'checklist_id' => 'Номер обходного листа',
            'checklistDate' => 'Дата обходного листа',
            'criterion_id' => 'Критерий оценки',
            'criterionName' => 'Критерий оценки',
            'workObjectName' => 'Объект обработки',
            'value' => 'Оценка',
        ];
    }

    /**
     * Gets query for [[Checklist]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChecklist()
    {
        return $this->hasOne(Checklists::className(), ['id' => 'checklist_id']);
    }

    /**
     * @return int
     */
    public function getChecklistDate()
    {
        return $this->checklist->created_at;
    }

    /**
     * Gets query for [[Criterion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCriterion()
    {
        return $this->hasOne(MarkCriteria::className(), ['id' => 'criterion_id']);
    }

    /**
     * @return string|null
     */

    public function getCriterionName()
    {
        return $this->criterion->name;
    }

    /**
     * @return string
     */
    public function getWorkObjectName()
    {
        return $this->criterion->workObjectName;
    }


}
