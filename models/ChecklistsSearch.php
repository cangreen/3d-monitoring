<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Checklists;

/**
 * ChecklistsSearch represents the model behind the search form of `app\models\Checklists`.
 */
class ChecklistsSearch extends Checklists
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'station_id', 'brigade_id', 'customer_id', 'examination_type_id', 'work_type_id', 'work_subtype_id', 'index', 'created_at', 'updated_at'], 'integer'],
      [['city', 'district', 'region', 'street', 'house', 'flat', 'address_full'], 'safe'],
      [['lat', 'lon'], 'number'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $user = Yii::$app->user;
    $query = Checklists::find();

    if(!$user->can('superadmin')) {
        $query->andWhere(['station_id' => $user->identity->station_id]);
    }


    // add conditions that should always apply here

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
    ]);

    $this->load($params);

    if (!empty($this->created_at)) {
      $this->filterByDate('created_at', $query);
    }


    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'station_id' => $this->station_id,
      'brigade_id' => $this->brigade_id,
      'customer_id' => $this->customer_id,
      'examination_type_id' => $this->examination_type_id,
      'work_type_id' => $this->work_type_id,
      'work_subtype_id' => $this->work_subtype_id,
      'index' => $this->index,
      'lat' => $this->lat,
      'lon' => $this->lon,
      'created_at' => $this->created_at,
      'updated_at' => $this->updated_at,
    ]);

    $query->andFilterWhere(['like', 'city', $this->city])
      ->andFilterWhere(['like', 'district', $this->district])
      ->andFilterWhere(['like', 'region', $this->region])
      ->andFilterWhere(['like', 'street', $this->street])
      ->andFilterWhere(['like', 'house', $this->house])
      ->andFilterWhere(['like', 'flat', $this->flat])
      ->andFilterWhere(['like', 'address_full', $this->address_full]);

    return $dataProvider;
  }

  private function filterByDate($attr, $query)
  {
    $dayStart = (int)\Yii::$app->formatter->asTimestamp($this->$attr . ' 00:00:00');
    $dayStop = (int)\Yii::$app->formatter->asTimestamp($this->$attr . ' 23:59:59');
    $query->andFilterWhere([
      'between',
      self::tableName() . ".$attr",
      $dayStart,
      $dayStop,
    ]);
  }

}
