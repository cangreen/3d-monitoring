<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_objects".
 *
 * @property int $id
 * @property string $name
 * @property int $examination_type_id
 * @property int $work_type_id
 * @property int $work_subtype_id
 *
 * @property MarkCriteria[] $markCriterias
 * @property ExaminationTypes $examinationType
 * @property string $examinationTypeName
 * @property WorkTypes $workType
 * @property string $workTypeName
 * @property WorkSubtypes $workSubtype
 * @property string $workSubtypeName
 */
class WorkObjects extends \yii\db\ActiveRecord
{
  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'work_objects';
  }

  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['name', 'examination_type_id', 'work_type_id', 'work_subtype_id'], 'required'],
      [['examination_type_id', 'work_type_id', 'work_subtype_id'], 'integer'],
      [['name'], 'string', 'max' => 32],
      [['examination_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExaminationTypes::className(), 'targetAttribute' => ['examination_type_id' => 'id']],
      [['work_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkTypes::className(), 'targetAttribute' => ['work_type_id' => 'id']],
      [['work_subtype_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkSubtypes::className(), 'targetAttribute' => ['work_subtype_id' => 'id']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'name' => 'Название',
      'examination_type_id' => 'Тип обследования',
      'examinationTypeName' => 'Тип обследования',
      'work_type_id' => 'Вид работы',
      'workTypeName' => 'Вид работы',
      'work_subtype_id' => 'Подвид работы ',
      'workSubtypeName' => 'Подвид работы ',
    ];
  }

  /**
   * Gets query for [[MarkCriterias]].
   *
   * @return \yii\db\ActiveQuery
   */
  public function getMarkCriterias()
  {
    return $this->hasMany(MarkCriteria::className(), ['work_object_id' => 'id']);
  }

  /**
   * Gets query for [[ExaminationType]].
   *
   * @return \yii\db\ActiveQuery
   */
  public function getExaminationType()
  {
    return $this->hasOne(ExaminationTypes::className(), ['id' => 'examination_type_id']);
  }

  /**
   * @return string
   */

  public function getExaminationTypeName()
  {
    return $this->examinationType->name;
  }


  /**
   * Gets query for [[WorkType]].
   *
   * @return \yii\db\ActiveQuery
   */
  public function getWorkType()
  {
    return $this->hasOne(WorkTypes::className(), ['id' => 'work_type_id']);
  }

  /**
   * @return string
   */
  public function getWorkTypeName()
  {
    return $this->workType->name;
  }

  /**
   * Gets query for [[WorkSubtype]].
   *
   * @return \yii\db\ActiveQuery
   */
  public function getWorkSubtype()
  {
    return $this->hasOne(WorkSubtypes::className(), ['id' => 'work_subtype_id']);
  }


  /**
   * @return string
   */
  public function getWorkSubtypeName()
  {
    return $this->workSubtype->name;
  }
}
