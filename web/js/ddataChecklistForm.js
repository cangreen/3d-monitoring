"use strict";

function showSelected(suggestion) {

  const data = suggestion.data;

  console.log(data);

  if (data.postal_code) {
    setField(data.postal_code, '#checklists-index');
  }

  if (data.city) {
    setField(data.city, '#checklists-city');
  }

  if (data.city_area) {
    setField(data.city_area, '#checklists-district');
  }

  if (data.city_district_with_type) {
    setField(data.city_district_with_type, '#checklists-region');
  }

  if (data.street_with_type) {
    setField(data.street_with_type, '#checklists-street');
  }
  
  if (data.house) {
    let string = data.house;
    if(data.block) {
      string += ` ${data.block_type} ${data.block}` ;
    }
    $('#checklists-house').val(string);
  }

  if (data.street_with_type) {
    setField(data.street_with_type, '#checklists-street');
  }

  if(data.flat) {
    setField(data.flat, '#checklists-flat');
  }

  if (data.geo_lat) {
    setField(data.geo_lat, '#checklists-lat');
  }
  if (data.geo_lon) {
    setField(data.geo_lon, '#checklists-lon');
  }

}

function setField(value, fieldSelector) {
  $(fieldSelector).val(value);
}

setTimeout(function () {

  let search = $('.suggestions-input').suggestions();

  search.setOptions({

    onSelect: showSelected

  });

}, 300);


