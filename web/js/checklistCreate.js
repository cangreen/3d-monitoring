"use strict";

const examinationTypeField = $('#checklists-examination_type_id');
const workTypeField = $('#checklists-work_type_id');
const workSubTypeField = $('#checklists-work_subtype_id');

let examination_type_id = examinationTypeField.val();
let work_type_id = workTypeField.val();
let work_subtype_id = workSubTypeField.val();

examinationTypeField.on('select2:select', function () {
    showMarks();
});
workTypeField.on('select2:select', function () {
    showMarks();
});
workSubTypeField.on('select2:select', function () {
    showMarks();
});


$('form#w0').submit(function (event) {

    console.log(event);

    const $emptyMarks = $('.form_radio_btn input[value=""]:checked');

    if ($emptyMarks.length !== 0) {

        event.preventDefault();

        const alert = ` 
               <div class="alert alert-danger alert-dismissible" style="margin-top: 20px;">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  Пожалуйста, заполните все оценки
              </div>`

        $('#checklist-marks .box-alert').html(alert);

        $emptyMarks.each(function (el) {

            const $el = $($emptyMarks[el]);

            $el.parent().prev().addClass('error-label');
        });
    }

});

function showMarks() {
    let examination_type_id = examinationTypeField.val();
    let work_type_id = workTypeField.val();
    let work_subtype_id = workSubTypeField.val();

    if (work_type_id !== '' && work_subtype_id !== '' && examination_type_id !== '') {
        $('#checklist-marks').show();
        $.get(
            '/api/work-objects',
            {
                examination_type_id: examination_type_id,
                work_type_id: work_type_id,
                work_subtype_id: work_subtype_id
            },
            function (data) {
                renderMarks(JSON.parse(data).data, JSON.parse(data).default);
                addTabHandler();
            }
        );
    }
}

function renderMarks(marks, defaultValue) {
    let html = '';
    for (let key in marks) {
        const criteria = marks[key].criteria;
        html +=
            `
              <h4 class="box-title">${marks[key].name}</h4>
                <div class="row">
            `;
        criteria.forEach(function (el) {

            const defaults = {
                0: '',
                1: '',
                2: '',
                3: '',
                4: '',
            }

            for (const key in defaults) {
                if (key === defaultValue) {
                    defaults[key] = 'checked'
                }
            }
            html +=
                `
                     <div class="form-group col-md-4">
                         <label>${el.name}</label>  
                         <div class="form_radio_btn">
                             <input id="mark_criteria_${el.id}_0" type="radio" name="MarkCriteria[${el.id}]" value="" checked>
                         </div>
                         <div class="form_radio_btn" tabindex="0">
                            <input id="mark_criteria_${el.id}_1" type="radio" name="MarkCriteria[${el.id}]" value="0" ${defaults[0]}>
                            <label for="mark_criteria_${el.id}_1">0</label>
                         </div>
                         <div class="form_radio_btn" tabindex="0">
                            <input id="mark_criteria_${el.id}_2" type="radio" name="MarkCriteria[${el.id}]" value="1" ${defaults[1]}>
                            <label for="mark_criteria_${el.id}_2">1</label>
                         </div>
                         <div class="form_radio_btn" tabindex="0">
                             <input id="mark_criteria_${el.id}_3" type="radio" name="MarkCriteria[${el.id}]" value="2" ${defaults[2]}>
                             <label for="mark_criteria_${el.id}_3">2</label>
                         </div>
                         <div class="form_radio_btn" tabindex="0">
                             <input id="mark_criteria_${el.id}_4" type="radio" name="MarkCriteria[${el.id}]" value="3" ${defaults[3]}>
                             <label for="mark_criteria_${el.id}_4">3</label>
                         </div>
                         <div class="form_radio_btn" tabindex="0">
                             <input id="mark_criteria_${el.id}_5" type="radio" name="MarkCriteria[${el.id}]" value="4" ${defaults[4]}>
                             <label for="mark_criteria_${el.id}_5">4</label>
                         </div> 
                      </div>   
                    `;
        });

        html += '</div>';
    }

    $('#checklist-marks .box-body').html(html);

    $('.form_radio_btn input[type="radio"]').change(function (event) {
        const $label = $(event.target).parent().parent().children().first();
        if ($label.hasClass('error-label')) {
            $label.removeClass('error-label');
        }
    });
}


function addTabHandler() {
    $(document).on('keydown', function (e) {
        if(e.keyCode === 32) {
            e.preventDefault();
            $(document.activeElement).find('label').trigger('click');
        }
    })
}

