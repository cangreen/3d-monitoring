"use strict";

const selected = $('.user-role-input option:selected');
if (selected.val() === 'superadmin') {
    $('.user-object-input').attr('disabled', true);
}


$('.user-role-input').change(function () {
    const selected = $('.user-role-input option:selected');
    if (selected.val() === 'superadmin') {
        $('.user-object-input').attr('disabled', true);
    } else {
        $('.user-object-input').attr('disabled', false);
    }
});
