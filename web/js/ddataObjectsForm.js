"use strict";

function showSelected(suggestion) {

  const data = suggestion.data;

  if (data.postal_code) {
    setField(data.postal_code, '#stations-index');
  }

  if (data.city) {
    setField(data.city, '#stations-city');
  }

  if (data.city_area) {
    setField(data.city_area, '#stations-district');
  }

  if (data.city_district_with_type) {
    setField(data.city_district_with_type, '#stations-region');
  }

  if (data.street_with_type) {
    setField(data.street_with_type, '#stations-street');
  }
  
  if (data.house) {
    let string = data.house;
    if(data.block) {
      string += ` ${data.block_type} ${data.block}` ;
    }
    $('#stations-house').val(string);
  }

  if (data.street_with_type) {
    setField(data.street_with_type, '#stations-street');
  }

  if(data.flat) {
    setField(data.flat, '#stations-flat');
  }

}

function setField(value, fieldSelector) {
  $(fieldSelector).val(value);
}

setTimeout(function () {

  let search = $('.suggestions-input').suggestions();

  search.setOptions({

    onSelect: showSelected

  });

}, 300);


