"use strict";

function showSelected(suggestion) {

  const data = suggestion.data;
  console.log(data);

  if (data.name.short_with_opf) {
    setField(data.name.short_with_opf, '#customers-name');
  }

  if (data.inn) {
    setField(data.inn, '#customers-tax_number');
  }

  if (data.address.data.postal_code) {
    setField(data.address.data.postal_code, '#customers-index');
  }

  if (data.address.data.city_with_type) {
    setField(data.address.data.city_with_type, '#customers-city');
  }

  if (data.address.data.city_area) {
    setField(data.address.data.city_area, '#customers-district');
  }

  if (data.address.data.city_district_with_type) {
    setField(data.address.data.city_district_with_type, '#customers-region');
  }
  

  if (data.address.data.street_with_type) {
    setField(data.address.data.street_with_type, '#customers-street');
  }
  
  if (data.address.data.house) {
    let string = data.address.data.house;
    if(data.address.data.block) {
      string += ` ${data.address.data.block_type} ${data.address.data.block}`;
    }
    $('#customers-house').val(string);
  }

  if(data.address.data.flat) {
    setField(data.address.data.flat, '#customers-flat');
  }

}

function setField(value, fieldSelector) {
  $(fieldSelector).val(value);
}

setTimeout(function () {

  let search = $('.suggestions-input').suggestions();

  search.setOptions({

    onSelect: showSelected

  });

}, 300);


