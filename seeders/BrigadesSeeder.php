<?php


namespace app\seeders;


use app\models\Brigades;
use app\models\Stations;
use Faker\Factory;
use yii\helpers\ArrayHelper;

class BrigadesSeeder extends Seeder
{

    protected function seed()
    {
        $faker = Factory::create('ru_RU');

        $result = [
            'models' => 0,
            'errors' => []
        ];

        for ($i = 1; $i < 51; $i++) {
            $model = new Brigades();
            $model->number = (string)$i;
            $model->station_id = $faker->randomElement($this->getArrayOfIds(new Stations()));
            if ($model->save()) {
                $result['models']++;
            } else {
                $result['errors'][] = $model->errors;
            }
        }

        return $result;
    }
}