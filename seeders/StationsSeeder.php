<?php

namespace app\seeders;

use app\models\Stations;
use Faker\Factory;
use yii\db\ActiveRecord;

/**
 * Class StationsSeeder
 * @package app\seeders
 */
class StationsSeeder extends Seeder
{
    protected function seed()
    {
        $faker = Factory::create('ru_RU');

        $result = [
            'models' => 0,
            'errors' => []
        ];

        for ($i = 1; $i < 50; $i++) {
            $model = new Stations();

            $model->name = "Дезинфекционная станция №0{$i}";
            $model->number = $i;
            $model->director_name = $faker->firstName();
            $model->director_surname = $faker->lastName;
            $model->director_patronymic = $faker->firstName();
            $model->phone = "+7{$faker->numberBetween(1000000000, 9999999999)}";
            $model->email = $faker->companyEmail;
            $model->index = $faker->postcode;
            $model->city = $faker->city;
            $model->district = $faker->city;
            $model->region = $faker->city;
            $model->street = $faker->streetName;
            $model->house = $faker->buildingNumber;
            $model->flat = (string)$faker->numberBetween(1, 300);
            $model->active = 1;

            $model->save() ? $result['models']++ : $result['errors'][] = $model->errors;
        }

        return $result;
    }
}
