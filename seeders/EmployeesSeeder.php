<?php


namespace app\seeders;


use app\models\Brigades;
use app\models\Employees;
use Faker\Factory;

class EmployeesSeeder extends Seeder
{

    protected function seed()
    {
        $faker = Factory::create('ru_RU');

        $result = [
            'models' => 0,
            'errors' => []
        ];

        for ($i = 1; $i < 501; $i++) {
            $model = new Employees();
            $model->name = $faker->firstName();
            $model->surname = $faker->lastName;
            $model->patronymic = $faker->firstName();
            $model->brigade_id = $faker->randomElement($this->getArrayOfIds(new Brigades()));
            $model->save() ? $result['models']++ : $result['errors'][] = $model->errors;
        }

        return $result;
    }
}