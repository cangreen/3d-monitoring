<?php


namespace app\seeders;


use app\models\MarkCriteria;
use app\models\Marks;
use app\models\Checklists;
use app\models\WorkObjects;
use Faker\Factory;

class MarksSeeder extends Seeder
{

    protected function seed()
    {
        $faker = Factory::create('ru_RU');

        $result = [
            'models' => 0,
            'errors' => []
        ];

        $checklists = Checklists::find()->all();

        foreach ($checklists as $checklist) {

            /** @var $checklist Checklists */

            $workObjects = WorkObjects::find()->where([
                'examination_type_id' => $checklist->examination_type_id,
                'work_type_id' => $checklist->work_type_id,
                'work_subtype_id' => $checklist->work_subtype_id,
            ])->all();

            if (!empty($workObjects)) {
                foreach ($workObjects as $workObject) {

                    /** @var $workObject WorkObjects */

                    $criteria = MarkCriteria::find()->where([
                        'work_object_id' => $workObject->id
                    ])->all();

                    foreach ($criteria as $criterion) {

                        /** @var $criterion MarkCriteria */

                        $model = new Marks();
                        $model->checklist_id = $checklist->id;
                        $model->criterion_id = $criterion->id;
                        $model->value = $faker->randomElement(['1', '2', '3', '4']);

                        $model->save() ? $result['models']++ : $result['errors'][] = $model->errors;
                    }
                }
            }
        }

        return $result;
    }
}