<?php


namespace app\seeders;


use app\models\Addresses;
use app\models\Brigades;
use app\models\Checklists;
use app\models\Customers;
use app\models\ExaminationTypes;
use app\models\Stations;
use app\models\WorkSubtypes;
use app\models\WorkTypes;
use Faker\Factory;
use Faker\Provider\Address;
use SebastianBergmann\CodeCoverage\Report\PHP;

class ChecklistsFromAddressesSeeder extends Seeder
{

    protected function seed()
    {
        $result = [
            'models' => 0,
            'errors' => []
        ];

        $faker = Factory::create('ru_RU');

        $addresses = Addresses::find()->all();

        foreach ($addresses as $address) {
            foreach (ExaminationTypes::find()->all() as $examinationType) {
                foreach (WorkTypes::find()->all() as $workType) {
                    foreach (WorkSubtypes::find()->all() as $workSubType) {
                        /** @var $address Addresses */
                        /** @var $examinationType ExaminationTypes */
                        /** @var $workType WorkTypes */
                        /** @var $workSubType WorkSubTypes */

                        $model = new Checklists();
                        $model->detachBehavior('timestamp');
                        $fakerAddress = new Address($faker);
                        $station_id = $faker->randomElement($this->getArrayOfStationIds(new Stations()));
                        $model->station_id = $station_id;
                        $brigadeId = $faker->randomElement($this->getArrayOfIds(new Brigades(), ['station_id' => $station_id]));
                        $model->brigade_id = $brigadeId ;
                        $model->customer_id = $faker->randomElement($this->getArrayOfIds(new Customers()));
                        $model->examination_type_id = $examinationType->id;
                        $model->work_type_id = $workType->id;
                        $model->work_subtype_id = $workSubType->id;
                        $model->index = $fakerAddress::postcode();
                        $model->city = 'Москва';
                        $model->district = $address->district;
                        $model->region = $address->region;
                        $model->street = $address->street;
                        $model->house = (string)$address->building;
                        $model->lat = $fakerAddress::latitude();
                        $model->lon = $fakerAddress::longitude();
                        $model->address_full = "Москва {$address->district} {$address->region} {$address->building}";
                        $model->created_at = $faker->dateTimeBetween('-30 days', 'now')->getTimestamp();
                        if ($model->save()) {
                            $result['models']++;
                        } else {
                            $result['errors'][] = $model->errors;
                        }

                    }
                }
            }
        }
        return $result;
    }
}