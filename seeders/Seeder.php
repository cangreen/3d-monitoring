<?php


namespace app\seeders;

use app\models\Stations;
use yii\db\ActiveRecord;

/**
 * Class Seeder
 * @package app\seeders
 */
abstract class Seeder
{

    /**
     * @return mixed
     */
    abstract protected function seed();

    /**
     * @return mixed
     */
    public function init()
    {
        return $this->seed();
    }

    /**
     * @param ActiveRecord $model
     * @param array|null $where
     * @return array
     */
    protected function getArrayOfIds(ActiveRecord $model, $where = null)
    {
        $result = [];
        $query = $model::find()->select('id');
        if (!is_null($where)) {
            $query->andWhere($where);
        }
        $models = $query->asArray()->all();

        foreach ($models as $record) {
            $result[] = $record['id'];
        }

        return $result;
    }

    protected function getArrayOfStationIds(Stations $model)
    {
        $result = [];
        $query = $model::find()->select('id');

        $models = $query->all();

        foreach ($models as $record) {
            /** @var $record Stations */
            if($record->getBrigades()->count() != 0)
            $result[] = $record['id'];
        }

        return $result;
    }

}