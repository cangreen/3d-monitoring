<?php


namespace app\seeders;


use app\models\Brigades;
use app\models\Checklists;
use app\models\Customers;
use app\models\ExaminationTypes;
use app\models\Stations;
use app\models\WorkSubtypes;
use app\models\WorkTypes;
use Faker\Factory;
use Faker\Provider\Address;
use yii\behaviors\TimestampBehavior;

class ChecklistsSeeder extends Seeder
{

    protected function seed()
    {
        $faker = Factory::create('ru_RU');

        $result = [
            'models' => 0,
            'errors' => []
        ];

        for ($i = 1; $i < 101; $i++) {
            $model = new Checklists();
            $model->detachBehavior('timestamp');
            $address = new Address($faker);
            $station_id = $faker->randomElement($this->getArrayOfIds(new Stations()));
            $model->station_id = $station_id;
            $model->brigade_id = $faker->randomElement($this->getArrayOfIds(new Brigades(), ['station_id' => $station_id]));
            $model->customer_id = $faker->randomElement($this->getArrayOfIds(new Customers()));
            $model->examination_type_id = $faker->randomElement($this->getArrayOfIds(new ExaminationTypes()));
            $model->work_type_id = $faker->randomElement($this->getArrayOfIds(new WorkTypes()));
            $model->work_subtype_id = $faker->randomElement($this->getArrayOfIds(new WorkSubtypes()));
            $model->index = $address::postcode();
            $model->city = $address->city();
            $model->district = $address->city();
            $model->region = $address->city();
            $model->street = $address->streetName();
            $model->house = $address::buildingNumber();
            $model->flat = (string)$faker->numberBetween(1, 300);
            $model->lat = $address::latitude();
            $model->lon = $address::longitude();
            $model->address_full = $address->address();
            $model->created_at = $faker->dateTimeBetween('-30 days', 'now')->getTimestamp();

            if($model->save()) {
                $result['models']++ ;
            } else {
                $result['errors'][] = $model->errors ;
            }
        }
        return $result;
    }
}