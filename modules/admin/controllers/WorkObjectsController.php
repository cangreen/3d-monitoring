<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\WorkObjects;
use app\models\WorkObjectsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\ExaminationTypes;
use app\models\WorkTypes;
use app\models\WorkSubtypes;

/**
 * WorkObjectsController implements the CRUD actions for WorkObjects model.
 */
class WorkObjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['superadmin']
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all WorkObjects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WorkObjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'filterData' => $this->getFilterData()
        ]);
    }

    /**
     * Displays a single WorkObjects model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WorkObjects model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WorkObjects();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'filterData' => $this->getFilterData()
            ]);
        }
    }

    /**
     * Updates an existing WorkObjects model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'filterData' => $this->getFilterData()
            ]);
        }
    }

    /**
     * Deletes an existing WorkObjects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WorkObjects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WorkObjects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WorkObjects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Получаем массивы данных для фильтров в виджетах
     * @return array
     */
    private function getFilterData()
    {
        return [
            'examinationTypes' => ArrayHelper::map(ExaminationTypes::find()->all(), 'id', 'name'),
            'workTypes' => ArrayHelper::map(WorkTypes::find()->all(), 'id', 'name'),
            'workSubTypes' => ArrayHelper::map(WorkSubTypes::find()->all(), 'id', 'name')
        ];
    }
}
