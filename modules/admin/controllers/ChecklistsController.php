<?php

namespace app\modules\admin\controllers;

use app\models\Brigades;
use app\models\Customers;
use app\models\ExaminationTypes;
use app\models\Marks;
use app\models\Stations;
use app\models\WorkSubtypes;
use app\models\WorkTypes;
use Yii;
use app\models\Checklists;
use app\models\ChecklistsSearch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\ChecklistsComponent;
use app\components\SessionComponent;

/**
 * ChecklistsController implements the CRUD actions for Checklists model.
 */
class ChecklistsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['personal_area']
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Checklists models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChecklistsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'filterData' => $this->getFilterData()
        ]);
    }

    /**
     * Displays a single Checklists model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $user = Yii::$app->user;

        $marks = Marks::find()->where(['checklist_id' => $id])->all();

        $marksIndexed = ArrayHelper::map(
            ArrayHelper::toArray($marks, [
                'app\models\Marks' => [
                    'workObjectName',
                    'criterionName',
                    'value'
                ],
            ]),
            'criterionName',
            'value',
            'workObjectName');

        if(!$user->can('superadmin') && $user->identity->station_id !== $model->station_id) {
            throw new ForbiddenHttpException('Доступ к объектам других станций запрещён');
        }

        return $this->render('view', [
            'model' => $model,
            'marks' => $marksIndexed,
        ]);
    }

    /**
     * Creates a new Checklists model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = (new ChecklistsComponent())->getChecklist();

        $post = Yii::$app->request->post();
        $marks = $post['MarkCriteria'] ?? null;

        $transaction = Yii::$app->db->beginTransaction();

        if ($model->load($post) && $model->save() && !is_null($marks) && $model->saveMarks($marks)) {
            (new SessionComponent($model))->saveInfo();
            $transaction->commit();
            return $this->redirect(['index']);
        } else {
            $transaction->rollBack();
            if (!empty($post) && is_null($marks)) {
                Yii::$app->session->setFlash('empty_marks', 'Требуется выставить хотя бы одну оценку');
            }
            return $this->render('create', [
                'model' => $model,
                'filterData' => $this->getFilterData()
            ]);
        }
    }

    /**
     * Updates an existing Checklists model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        $marks = $post['MarkCriteria'] ?? null;
        $user = Yii::$app->user;

        if ($user->can('operator')) {
            throw new ForbiddenHttpException('Действие запрещено');
        }

        if(!$user->can('superadmin') && $user->identity->station_id !== $model->station_id) {
            throw new ForbiddenHttpException('Доступ к объектам других станций запрещён');
        }

        $transaction = Yii::$app->db->beginTransaction();

        if ($model->load($post) && $model->save() && !is_null($marks) && $model->saveMarks($marks, $model->id)) {
            $transaction->commit();
            return $this->redirect(['index']);
        } else {
            $transaction->rollBack();
            return $this->render('update', [
                'model' => $model,
                'filterData' => $this->getFilterData(),
                'marks' => $model->getMarkCriteriaWithMarks()
            ]);
        }
    }

    /**
     * Deletes an existing Checklists model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Checklists model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Checklists the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Checklists::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function getFilterData()
    {
        $brigadesQuery = Brigades::find();
        $stationsQuery = Stations::find();
        $user = Yii::$app->user;
        if(!$user->can('superadmin')) {
            $brigadesQuery->where(['station_id' => $user->identity->station_id]);
            $stationsQuery->where(['id' => $user->identity->station_id]);
        }
        return [
            'stations' => ArrayHelper::map($stationsQuery->all(), 'id', 'name'),
            'brigades' => ArrayHelper::map($brigadesQuery->all(), 'id', 'number'),
            'customers' => ArrayHelper::map(Customers::find()->all(), 'id', 'name'),
            'examinationTypes' => ArrayHelper::map(ExaminationTypes::find()->all(), 'id', 'name'),
            'workTypes' => ArrayHelper::map(WorkTypes::find()->all(), 'id', 'name'),
            'workSubTypes' => ArrayHelper::map(WorkSubTypes::find()->all(), 'id', 'name')
        ];
    }

    /**
     * @return \yii\web\Response
     */
    public function actionClear()
    {
        if(Yii::$app->request->isPost) {
            Yii::$app->session->set('info', '');
            return $this->redirect('/checklists/create');
        };
    }

}
