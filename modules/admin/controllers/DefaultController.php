<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => '\yii\filters\AccessControl',
        'rules' => [
          [
            'allow' => true,
            'roles' => ['personal_area']
          ],
        ],
      ],

    ];
  }

  /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
      $user = Yii::$app->user->identity;
      $role = Yii::$app->authManager->getRolesByUser($user->id);
      $roleDesc = '';
      foreach ($role as $value) {
        $roleDesc = $value->description;
      }
        return $this->render('index', [
          'user' => $user,
          'role' => $roleDesc
        ]);
    }
}
