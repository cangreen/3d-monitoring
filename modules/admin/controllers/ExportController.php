<?php

namespace app\modules\admin\controllers;

use app\models\Brigades;
use app\models\Customers;
use app\models\ExaminationTypes;
use app\models\MarksSearch;
use app\models\Stations;
use app\models\WorkSubtypes;
use app\models\WorkTypes;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;


class ExportController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['personal_area']
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new MarksSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);
        if (empty($queryParams)) {
            $startParams['MarksSearch']['nullMarks'] = '0';
            $dataProvider = $searchModel->search($startParams);
        }
        $sort = $dataProvider->getSort();
        unset($sort->attributes['criterionName']);
        unset($sort->attributes['value']);
        $sort->defaultOrder = ['checklistDate' => SORT_DESC];
        $dataProvider->setPagination(['pageSize' => 100]);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'filterData' => $this->getFilterData()
        ]);
    }

    private function getFilterData()
    {
        return [
            'stations' => ArrayHelper::map(Stations::find()->all(), 'id', 'name'),
            'brigades' => ArrayHelper::map(Brigades::find()->all(), 'id', 'number'),
            'customers' => ArrayHelper::map(Customers::find()->all(), 'id', 'name'),
            'examinationTypes' => ArrayHelper::map(ExaminationTypes::find()->all(), 'id', 'name'),
            'workTypes' => ArrayHelper::map(WorkTypes::find()->all(), 'id', 'name'),
            'workSubTypes' => ArrayHelper::map(WorkSubTypes::find()->all(), 'id', 'name')
        ];
    }

}
