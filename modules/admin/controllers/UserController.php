<?php

namespace app\modules\admin\controllers;

use app\models\AuthItem;
use app\models\Stations;
use app\models\UserCreate;
use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
      'access' => [
        'class' => '\yii\filters\AccessControl',
        'rules' => [
          [
            'allow' => true,
            'roles' => ['superadmin']
          ],
        ],
      ],
    ];
  }

  /**
   * Lists all User models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new UserSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $stations[0] = 'Не задана';
    $stations = $stations + ArrayHelper::map(Stations::find()->all(), 'id', 'name');

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'roles' => ArrayHelper::map(
        AuthItem::find()->where(['type' => 1])->all(),
        'name', 'description'
      ),
      'stations' => $stations
    ]);
  }

  /**
   * Displays a single User model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new User model.
   * If creation is successful, the browser will be redirected to the 'index' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new UserCreate();
    $post = Yii::$app->request->post();
    $stations = ArrayHelper::map(Stations::find()->all(), 'id', 'name');
    $roles = ArrayHelper::map(
      AuthItem::find()->where(['type' => 1])->all(),
      'name', 'description'
    );

    if ($model->load($post) && $model->create()) {
      return $this->redirect(['index']);
    } else {
      return $this->render('create', [
        'model' => $model,
        'stations' => $stations,
        'roles' => $roles
      ]);
    }
  }

  /**
   * Updates an existing User model.
   * If update is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $stations = ArrayHelper::map(Stations::find()->all(), 'id', 'name');
    $roles = ArrayHelper::map(
      AuthItem::find()->where(['type' => 1])->all(),
      'name', 'description'
    );

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    } else {
      return $this->render('update', [
        'stations' => $stations,
        'roles' => $roles,
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing User model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }


  /**
   * Finds the User model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return User the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionChangePassword($id)
  {
    $model = $this->findModel($id);
    $post = Yii::$app->request->post();
    if (isset($post['password']) && $model->setPassword($post['password']) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('change-password', [
        'model' => $model,
      ]);
    }
  }

}
