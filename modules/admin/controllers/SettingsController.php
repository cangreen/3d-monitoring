<?php

namespace app\modules\admin\controllers;

use app\models\Settings;
use app\models\Stations;
use Yii;
use app\models\Brigades;
use app\models\BrigadesSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['superadmin']
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $post = Yii::$app->request->post('Settings');

        $default_mark = Settings::find()->where(['name' => 'default_mark'])->one();

        if (!empty($post['default_mark']) || $post['default_mark'] === '0') {
            $default_mark->value = $post['default_mark'];
        }
        if ($post['default_mark'] === '') {
            $default_mark->value = null;
        }

        $default_mark->save();

        return $this->render('index', [
            'default_mark' => $default_mark
        ]);
    }
}
