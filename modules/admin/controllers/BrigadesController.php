<?php

namespace app\modules\admin\controllers;

use app\models\Stations;
use Yii;
use app\models\Brigades;
use app\models\BrigadesSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * BrigadesController implements the CRUD actions for Brigades model.
 */
class BrigadesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['personal_area']
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Brigades models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrigadesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Brigades model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $user = Yii::$app->user;
        if(!$user->can('superadmin') && $user->identity->station_id !== $model->station_id) {
            throw new ForbiddenHttpException('Доступ к объектам других станций запрещён');
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Brigades model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Brigades();
        $user = Yii::$app->user;
        if (!$user->can('superadmin')) {
            $model->station_id = $user->identity->station_id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            $query = Stations::find();
            if(!$user->can('superadmin')) {
                $query->where(['id' => $user->identity->station_id]);
            }
            return $this->render('create', [
                'model' => $model,
                'stations' => ArrayHelper::map($query->all(), 'id', 'name')
            ]);
        }
    }

    /**
     * Updates an existing Brigades model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = Yii::$app->user;

        if(!$user->can('superadmin') && $user->identity->station_id !== $model->station_id) {
            throw new ForbiddenHttpException('Доступ к объектам других станций запрещён');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            $query = Stations::find();
            if(!$user->can('superadmin')) {
                $query->where(['id' => $user->identity->station_id]);
            }

            return $this->render('update', [
                'model' => $model,
                'stations' => ArrayHelper::map($query->all(), 'id', 'name')
            ]);
        }
    }

    /**
     * Deletes an existing Brigades model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Brigades model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Brigades the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Brigades::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return array
     */
    public function actionGetBrigades()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        $parents = $_POST['depdrop_parents'];
        $station_id = $parents[0];
        if (isset($parents) && !empty($station_id)) {
            $array = ArrayHelper::toArray(Brigades::find()->where(['station_id' => $station_id])->all());
            foreach ($array as $value) {
                $out[] = ['id' => $value['id'], 'name' => $value['number']];
            }
            return ['output' => $out, 'selected' => $station_id];

        }
        return ['output' => '', 'selected' => ''];
    }
}
