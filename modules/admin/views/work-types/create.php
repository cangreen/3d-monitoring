<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WorkTypes */
/* @var $filterData array */

$this->title = 'Создание вида работ';
$this->params['breadcrumbs'][] = ['label' => 'Виды работ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-types-create">

  <?= $this->render('_form', [
    'model' => $model,
    'filterData' => $filterData
  ]) ?>

</div>
