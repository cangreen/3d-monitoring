<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkTypes */

$this->title = 'Редактирование вида работ: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Виды работ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="work-types-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
