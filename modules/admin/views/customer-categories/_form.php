<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerCategories */
/* @var $form yii\widgets\ActiveForm */
/* @var $categories array */

?>

<div class="customer-categories-form box box-primary">
  <style>
    .select2-container--krajee .select2-selection {
      border-radius: unset !important;
    }
  </style>
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'parent_id')->widget(Select2::classname(), [
          'data' => $categories,
          'options' => ['placeholder' => 'Выберите категорию ...'],
          'pluginOptions' => [
            'allowClear' => true
          ],
        ])?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success
      btn-flat']) ?>
      <?= Html::a('Отменить', '/customer-categories/index',
      ['class' => 'btn
      btn-warning
      btn-flat']) ?>

    </div>
    <?php ActiveForm::end(); ?>
</div>
