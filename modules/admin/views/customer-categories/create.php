<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerCategories */

$this->title = 'Создание Категории';
$this->params['breadcrumbs'][] = ['label' => 'Создание категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-categories-create">

    <?= $this->render('_form', [
    'model' => $model,
    'categories' => $categories,
    ]) ?>

</div>
