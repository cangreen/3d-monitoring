<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerCategories */

$this->title = 'Редактирование Категории: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории заказчиков', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="customer-categories-update">

  <?= $this->render('_form', [
    'model' => $model,
    'categories' => $categories,
  ]) ?>

</div>
