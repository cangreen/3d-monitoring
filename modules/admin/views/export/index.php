<?php

use app\models\Marks;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MarksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Экспорт';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'checklistDate',
        'value' => function (Marks $model) {
            return Yii::$app->formatter->asDate($model->checklistDate);
        },
    ],
    [
        'attribute' => 'examinationTypeName',
        'value' => function (Marks $model) {
            return $model->checklist->examinationTypeName;
        },
        'label' => 'Вид обследования',
    ],
    [
        'attribute' => 'stationName',
        'value' => function (Marks $model) {
            return $model->checklist->stationName;
        },
        'label' => 'Дез. станция',
    ],
    [
        'attribute' => 'brigadeNumber',
        'value' => function (Marks $model) {
            return $model->checklist->brigadeNumber;
        },
        'label' => 'Номер бригады',
    ],
    [
        'attribute' => 'customerName',
        'value' => function (Marks $model) {
            return $model->checklist->customerName;
        },
        'label' => 'Заказчик',
    ],
    [
        'attribute' => 'workTypeName',
        'value' => function (Marks $model) {
            return $model->checklist->workTypeName;
        },
        'label' => 'Вид работ',
    ],
    [
        'attribute' => 'workSubTypeName',
        'value' => function (Marks $model) {
            return $model->checklist->workSubtypeName;
        },
        'label' => 'Подвид работ',
    ],
    [
        'attribute' => 'district',
        'value' => function (Marks $model) {
            return $model->checklist->district;
        },
        'label' => 'Округ',
    ],
    [
        'attribute' => 'region',
        'value' => function (Marks $model) {
            return $model->checklist->region;
        },
        'label' => 'Район',
    ],
    [
        'attribute' => 'street',
        'value' => function (Marks $model) {
            return $model->checklist->street;
        },
        'label' => 'Улица',
    ],
    [
        'attribute' => 'house',
        'value' => function (Marks $model) {
            return $model->checklist->house;
        },
        'label' => 'Дом',
    ],
    [
        'value' => function (Marks $model) {
            return $model->checklist->lat;
        },
        'label' => 'Широта'
    ], [
        'value' => function (Marks $model) {
            return $model->checklist->lon;
        },
        'label' => 'Долгота'
    ],
    [
        'attribute' => 'workObjectName',
    ],

    'criterionName',
    'value'
];
$exportColumns = $gridColumns;
$exportColumns[] = [
    'value' => function(){return '1';},
    'label' => ''
];

?>
<div class="marks-index box box-primary">
    <div class="box-body">
        <?= $this->render('_search', ['model' => $searchModel]); ?>

        <?php $exportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $exportColumns,
            'batchSize' => 50,
            'target' => '_blank',
            'dropdownOptions' => [
                'label' => 'Экспорт',
                'class' => 'btn btn-outline-secondary'
            ],
            'exportConfig' => [
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_EXCEL => false,
                ExportMenu::FORMAT_EXCEL_X => false,
                ExportMenu::FORMAT_PDF => false,
            ]
        ])
        ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'krajeeDialogSettings' => ['useNative' => true],
            'striped' => true,
            'hover' => true,
            'toolbar' => [
                $exportMenu
            ],
            'export' => [
                'label' => 'Выгрузить',
                'showConfirmAlert' => false,
                'clearBuffers' => true,
                'header' => '',
            ],
            'columns' => $gridColumns,
            'showPageSummary' => false,
            'panel' => [
                'type' => GridView::TYPE_DEFAULT,
            ],
        ]); ?>
    </div>
</div>
