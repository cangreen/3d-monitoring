<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MarksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marks-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'dateFrom')->widget(DatePicker::class, [
                'model' => $model,
                'language' => 'ru',
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'dd.mm.yyyy',
                ],
            ]) ?>
        </div>
        <div class="col-md-3 "> <?= $form->field($model, 'dateTo')->widget(DatePicker::class, [
                'model' => $model,
                'language' => 'ru',
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'dd.mm.yyyy',
                ],
            ]) ?></div>
    </div>

    <?= $form->field($model, 'nullMarks')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
