<?php

/**
 * @var $this yii\web\View
 * @var $user \app\models\User
 * @var $role string
 */

$this->title = 'Личный кабинет';
?>

<div class="admin-default-index">
  <div class="box">
    <div class="box-header">

    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
      <table class="table table-hover">
        <tbody>
        <tr>
          <th>ID</th>
          <td><?= $user->id ?></td>
        </tr>
        <tr>
          <th>Email</th>
          <td><?= $user->email ?></td>
        </tr>
        <tr>
          <th>Фамилия</th>
          <td><?= $user->surname ?></td>
        </tr>
        <tr>
          <th>Имя</th>
          <td><?= $user->name ?></td>
        </tr>
        <tr>
          <th>Отчество</th>
          <td><?= $user->patronymic ?></td>
        </tr>
        <tr>
          <th>Телефон</th>
          <td><?= $user->phone ?></td>
        </tr>
        <tr>
          <th>Роль</th>
          <td><?= $role ?></td>
        </tr>


        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</div>
