<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\MarkCriteria */
/* @var $form yii\widgets\ActiveForm */
/* @var $objects array */
?>

<div class="mark-criteria-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'work_object_id')->widget('\kartik\select2\Select2', [
                'data' => $objects,
                'options' => ['placeholder' => 'Выберите...'],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]
        )?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success
      btn-flat']) ?>
      <?= Html::a('Отменить', '/mark-criteria/index',
      ['class' => 'btn
      btn-warning
      btn-flat']) ?>

    </div>
    <?php ActiveForm::end(); ?>
</div>
