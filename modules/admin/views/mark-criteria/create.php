<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MarkCriteria */
/* @var $objects array */

$this->title = 'Создание критерия';
$this->params['breadcrumbs'][] = ['label' => 'Критерии оценки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mark-criteria-create">

    <?= $this->render('_form', [
        'model' => $model,
        'objects' => $objects

    ]) ?>

</div>
