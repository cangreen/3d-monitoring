<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MarkCriteria */
/* @var $objects array */

$this->title = 'Редактирование критерия: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Критерии оценки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="mark-criteria-update">

    <?= $this->render('_form', [
        'model' => $model,
        'objects' => $objects
    ]) ?>

</div>
