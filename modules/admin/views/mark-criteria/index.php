<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MarkCriteriaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Критерии оценки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mark-criteria-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Добавить критерий', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'name',
                'workObjectName',
                'examinationTypeName',
                'workTypeName',
                'workSubTypeName',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
