<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Stations;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Дезстанции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objects-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Добавить дезстанцию', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [

//                'id',
                'number',
                'name',
//                'director_name',
                'director_surname',
                // 'director_patronymic',
                 'phone',
                 'email:email',
//                 'index',
//                 'city',
//                 'district',
//                 'region',
//                 'house',
//                 'flat',
                 [
                     'attribute' => 'active',
                     'label' => 'Активен',
                      'filter' => [
                          0 => 'Нет',
                          1 => 'Да',
                      ],
                   'value' => function(Stations $model) {
                      return $model->active
                        ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>'
                        : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
                   },
                   'format' => 'raw'
                 ],
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
