<?php

use app\models\Stations;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use corpsepk\DaData\SuggestionsWidget;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\Stations */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<div class="objects-form box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Основная информация</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-md-6"><?= $form->field($model, 'number')->textInput() ?></div>
      <div class="col-md-6"> <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
      <div class="col-md-4"><?= $form->field($model, 'director_name')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-4"> <?= $form->field($model, 'director_surname')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-4"><?= $form->field($model, 'director_patronymic')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
      <div class="col-md-6"> <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-6">  <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
    </div>
    <?php $model->active = 1 ?>
    <?= $form->field($model, 'active')->widget(SwitchInput::class, [
      'pluginOptions'=>[
        'onText' => '<i class="glyphicon glyphicon-ok"></i>',
        'offText' => '<i class="glyphicon glyphicon-remove"></i>',
      ]
    ]) ?>

  </div>
</div>
<div class="objects-form box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Адрес</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">
    <?= Html::label('Поиск по адресу', null, [
      'class' => 'control-label'
    ]) ?>

    <?= SuggestionsWidget::widget([
      'name' => 'address-search',
      'type' => 'ADDRESS',
      'inputOptions' => [
        'placeholder' => 'Адрес в свободной форме',
        'onSelect' => null,
      ]
    ]) ?>
    <div class="row" style="margin-top: 20px;">
      <div class="col-md-4"><?= $form->field($model, 'index')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-4"><?= $form->field($model, 'city')->textInput() ?></div>
      <div class="col-md-4"> <?= $form->field($model, 'district')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
      <div class="col-md-4"> <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-4"> <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-2"><?= $form->field($model, 'house')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-2"><?= $form->field($model, 'flat')->textInput(['maxlength' => true]) ?></div>
    </div>
  </div>
</div>
<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success
      btn-flat']) ?>

<?= Html::a('Отменить', '/stations/index',
  ['class' => 'btn
      btn-warning
      btn-flat']) ?>
<?php ActiveForm::end(); ?>

<?php $this->registerJsFile('/js/ddataObjectsForm.js', ['depends' => 'yii\web\JqueryAsset'])?>

