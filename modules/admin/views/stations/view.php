<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Stations */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Объекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="objects-view box box-primary">
    <div class="box-header">
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class'
      => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'number',
                'name',
                'director_name',
                'director_surname',
                'director_patronymic',
                'phone',
                'email:email',
                'index',
                'city',
                'district',
                'region',
                'house',
                'flat',
                'active',
              [
                'attribute' => 'created_at',
                'value' => function (\app\models\Stations $model) {
                  return Yii::$app->formatter->asDatetime($model->created_at, 'php:d.m.Y H:i:s');
                },
                'format' => 'raw'
              ],
              [
                'attribute' => 'updated_at',
                'value' => function (\app\models\Stations $model) {
                  return Yii::$app->formatter->asDatetime($model->updated_at, 'php:d.m.Y H:i:s');
                },
                'format' => 'raw'
              ],
            ],
        ]) ?>
    </div>
</div>
