<?php

/* @var $this yii\web\View */
/* @var $model app\models\Stations */

$this->title = 'Редактирование Объекта: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Объекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="objects-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
