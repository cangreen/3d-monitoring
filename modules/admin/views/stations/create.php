<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Stations */


$this->title = 'Создание дезстанции';
$this->params['breadcrumbs'][] = ['label' => 'Дезстанции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objects-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
