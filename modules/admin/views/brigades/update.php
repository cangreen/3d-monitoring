<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Brigades */
/* @var $stations array */

$this->title = 'Редактирование Бригады: ' . $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Бригады', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->number, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="brigades-update">

    <?= $this->render('_form', [
        'model' => $model,
        'stations' => $stations
    ]) ?>

</div>
