<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Brigades */
/* @var $form yii\widgets\ActiveForm */
/* @var $stations array */
?>

<div class="brigades-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'station_id')->widget('\kartik\select2\Select2', [
                'data' => $stations,
                'options' => ['placeholder' => 'Выберите дез. станцию...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'disabled' => !Yii::$app->user->can('superadmin')
            ]
        ) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success
      btn-flat']) ?>
      <?= Html::a('Отменить', '/brigades/index',
      ['class' => 'btn
      btn-warning
      btn-flat']) ?>

    </div>
    <?php ActiveForm::end(); ?>
</div>
