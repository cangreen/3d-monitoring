<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BrigadesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Бригады';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brigades-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Добавить бригаду', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'id',
                'number',
                'stationName',

//                'created_at',
//                'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
