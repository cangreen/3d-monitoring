<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Brigades */
/* @var $stations array */

$this->title = 'Создание бригады';
$this->params['breadcrumbs'][] = ['label' => 'Бригады', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brigades-create">

    <?= $this->render('_form', [
        'model' => $model,
        'stations' => $stations
    ]) ?>

</div>
