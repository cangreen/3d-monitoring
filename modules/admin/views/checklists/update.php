<?php

use kartik\datecontrol\DateControl;
use kartik\widgets\DepDrop;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Checklists */
/* @var $filterData array */
/* @var $marks array */

$this->title = 'Редактирование Обходного листа: ' . Yii::$app->formatter->asDatetime($model->created_at);
$this->params['breadcrumbs'][] = ['label' => 'Обходные листы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->formatter->asDatetime($model->created_at), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="checklists-update">

    <?php $form = ActiveForm::begin(); ?>
    <div class="checklists-form box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Основные данные</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-3"><?= $form->field($model, 'created_at')->widget('kartik\datecontrol\DateControl',
                        [
                            'type' => DateControl::FORMAT_DATE,
                            'widgetOptions' => [
                                'pluginOptions' => [
                                    'autoclose' => true
                                ]
                            ]
                        ]) ?>
                </div>
                <div class="col-md-3">

                    <?= $form->field($model, 'examination_type_id')->widget('\kartik\select2\Select2', [
                            'data' => $filterData['examinationTypes'],
                            'options' => ['placeholder' => 'Выберите ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ]
                    ) ?>

                </div>
                <div class="col-md-6">

                    <?= $form->field($model, 'customer_id')->widget('\kartik\select2\Select2', [
                            'data' => $filterData['customers'],
                            'options' => ['placeholder' => 'Выберите ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ]
                    ) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"><?= $form->field($model, 'station_id')->widget('\kartik\select2\Select2', [
                            'data' => $filterData['stations'],
                            'options' => ['placeholder' => 'Выберите дезстанцию ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            'disabled' => !Yii::$app->user->can('superadmin')
                        ]
                    ); ?>
                </div>
                <?php if (Yii::$app->user->can('superadmin')): ?>
                    <div class="col-md-3"> <?= $form->field($model, 'brigade_id')->widget(DepDrop::class, [
                            'pluginOptions' => [
                                'depends' => ['checklists-station_id'],
                                'placeholder' => 'Выбор бригады...',
                                'url' => Url::to(['/brigades/get-brigades'])
                            ],

                        ]) ?>
                    </div>
                <?php else: ?>
                    <div class="col-md-3"> <?= $form->field($model, 'brigade_id')->widget('\kartik\select2\Select2', [
                                'data' => $filterData['brigades'],
                                'options' => ['placeholder' => 'Выберите ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ]
                            ]
                        ) ?>
                    </div>
                <?php endif; ?>
                <div class="col-md-3"> <?= $form->field($model, 'work_type_id')->widget('\kartik\select2\Select2', [
                            'data' => $filterData['workTypes'],
                            'options' => ['placeholder' => 'Выберите ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ]
                    ) ?>
                </div>
                <div class="col-md-3">

                    <?= $form->field($model, 'work_subtype_id')->widget('\kartik\select2\Select2', [
                            'data' => $filterData['workSubTypes'],
                            'options' => ['placeholder' => 'Выберите ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ]
                    ) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="checklists-form box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Данные обходного листа</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'address_full')->widget('corpsepk\DaData\SuggestionsWidget',
                        [
                            'type' => 'ADDRESS',
                            'inputOptions' => [
                                'placeholder' => 'Введите адрес',
                                'onSelect' => null,
                            ]
                        ])
                    ?></div>
                <div class="col-md-3"><?= $form->field($model, 'index')->textInput() ?></div>
                <div class="col-md-3"> <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?></div>
            </div>
            <div class="row">
                <div class="col-md-4"><?= $form->field($model, 'district')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-4"><?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-4"><?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?></div>
            </div>
            <div class="row">
                <div class="col-md-3"><?= $form->field($model, 'house')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-3"> <?= $form->field($model, 'flat')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-3"><?= $form->field($model, 'lat')->textInput() ?></div>
                <div class="col-md-3"> <?= $form->field($model, 'lon')->textInput() ?></div>
            </div>

        </div>

    </div>
    <div class="checklists-form box box-primary" id="checklist-marks">
        <div class="box-header with-border">
            <h3 class="box-title">Оценки</h3>
            <div class="box-alert"></div>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <?php foreach ($marks as $mark): ?>
                <h4 class="box-title"><?= $mark['name'] ?></h4>

                <div class="row">
                    <?php foreach ($mark['criteria'] as $value): ?>
                        <div class="form-group col-md-4">
                            <label><?= $value['name'] ?></label>
                            <div class="form_radio_btn" tabindex="0">
                                <input id="mark_criteria_<?= $value['id'] ?>_0" type="radio"
                                       name="MarkCriteria[<?= $value['id'] ?>]" value="0"
                                       <?php if ($value['mark'] === 0): ?>checked<?php endif; ?>>
                                <label for="mark_criteria_<?= $value['id'] ?>_0">0</label>
                            </div>
                            <div class="form_radio_btn" tabindex="0">
                                <input id="mark_criteria_<?= $value['id'] ?>_1" type="radio"
                                       name="MarkCriteria[<?= $value['id'] ?>]" value="1"
                                       <?php if ($value['mark'] === 1): ?>checked<?php endif; ?>>
                                <label for="mark_criteria_<?= $value['id'] ?>_1">1</label>
                            </div>
                            <div class="form_radio_btn" tabindex="0">
                                <input id="mark_criteria_<?= $value['id'] ?>_2" type="radio"
                                       name="MarkCriteria[<?= $value['id'] ?>]" value="2"
                                       <?php if ($value['mark'] === 2): ?>checked<?php endif; ?>>
                                <label for="mark_criteria_<?= $value['id'] ?>_2">2</label>
                            </div>
                            <div class="form_radio_btn" tabindex="0">
                                <input id="mark_criteria_<?= $value['id'] ?>_3" type="radio"
                                       name="MarkCriteria[<?= $value['id'] ?>]" value="3"
                                       <?php if ($value['mark'] === 3): ?>checked<?php endif; ?>>
                                <label for="mark_criteria_<?= $value['id'] ?>_3">3</label>
                            </div>
                            <div class="form_radio_btn" tabindex="0">
                                <input id="mark_criteria_<?= $value['id'] ?>_4" type="radio"
                                       name="MarkCriteria[<?= $value['id'] ?>]" value="4"
                                       <?php if ($value['mark'] === 4): ?>checked<?php endif; ?>>
                                <label for="mark_criteria_<?= $value['id'] ?>_4">4</label>
                            </div>
                        </div>

                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success
      btn-flat']) ?>

    <?= Html::a('Отменить', '/checklists/index',
        ['class' => 'btn
      btn-warning
      btn-flat']) ?>
    <?php ActiveForm::end(); ?>

    <?php $this->registerJsFile('/js/ddataChecklistForm.js', ['depends' => 'yii\web\JqueryAsset']) ?>
    <?php $this->registerJsFile('/js/checklistUpdate.js', ['depends' => 'yii\web\JqueryAsset']) ?>

</div>
