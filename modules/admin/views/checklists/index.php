<?php

use yii\grid\GridView;
use app\models\Checklists;
use kartik\date\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ChecklistsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $filterData array */

$this->title = 'Список обходных листов';
$this->params['breadcrumbs'][] = $this->title;
$actionColumnTemplate = !Yii::$app->user->can('operator') ? '{view} {update} {delete}' : '{view}';
?>
<div class="checklists-index box box-primary">
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [

                [
                    'attribute' => 'created_at',
                    'value' => function (Checklists $model) {
                        return Yii::$app->formatter->asDate($model->created_at);
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'language' => 'ru',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'format' => 'dd.mm.yyyy',
                        ],
                    ]),

                ],
                [
                    'attribute' => 'station_id',
                    'filter' => Select2::widget([
                        'attribute' => 'station_id',
                        'model' => $searchModel,
                        'data' => $filterData['stations'],
                        'options' => ['placeholder' => 'Выберите...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'disabled' => !Yii::$app->user->can('superadmin')
                    ]),
                    'value' => function (Checklists $model) {
                        return $model->station->name;
                    }
                ],
                [
                    'attribute' => 'brigade_id',
                    'filter' => Select2::widget([
                        'attribute' => 'brigade_id',
                        'model' => $searchModel,
                        'data' => $filterData['brigades'],
                        'options' => ['placeholder' => 'Выберите...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]),
                    'value' => function (Checklists $model) {
                        return $model->brigade->number;
                    }
                ],
                [
                    'attribute' => 'customer_id',
                    'filter' => Select2::widget([
                        'attribute' => 'customer_id',
                        'model' => $searchModel,
                        'data' => $filterData['customers'],
                        'options' => ['placeholder' => 'Выберите...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]),
                    'value' => function (Checklists $model) {
                        return $model->customer->name;
                    }
                ],
                [
                    'attribute' => 'examination_type_id',
                    'filter' => $filterData['examinationTypes'],
                    'value' => function (Checklists $model) {
                        return $model->examinationType->name;
                    }
                ],
                [
                    'attribute' => 'work_type_id',
                    'filter' => $filterData['workTypes'],
                    'value' => function (Checklists $model) {
                        return $model->workType->name;
                    }
                ],
                [
                    'attribute' => 'work_subtype_id',
                    'filter' => $filterData['workSubTypes'],
                    'value' => function (Checklists $model) {
                        return $model->workSubtype->name;
                    }
                ],
                // 'index',
                // 'city',
                // 'district',
                // 'region',
                // 'street',
                // 'house',
                // 'flat',
                // 'lat',
                // 'lon',
                'address_full',
                // 'active',
                // 'updated_at',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $actionColumnTemplate
                ]
            ],
        ]); ?>
    </div>
</div>
