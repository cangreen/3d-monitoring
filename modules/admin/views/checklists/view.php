<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Checklists */
/* @var $marks array */

$this->title = $model->examinationType->name . ' от ' . Yii::$app->formatter->asDatetime($model->created_at);
$this->params['breadcrumbs'][] = ['label' => 'обходные листы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="checklists-view box box-primary">
    <div class="box-header">
        <?php if (!Yii::$app->user->can('operator')): ?>
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class'
            => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'created_at:date',
                'stationName',
                'brigadeNumber',
                'customerName',
                'examinationTypeName',
                'workTypeName',
                'workSubTypeName',
                'index',
                'city',
                'district',
                'region',
                'street',
                'house',
                'flat',
                'lat',
                'lon',
                'address_full',
            ],
        ]) ?>
    </div>
</div>
<div class="checklists-view box box-primary">
    <div class="box-header"><h4>Оценки</h4></div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
            <?php foreach ($marks as $work_object => $criteria_marks) : ?>
                <tr>
                    <th colspan="2">
                        <div style="padding-left: 10px;"><?= $work_object ?></div>
                    </th>
                </tr>
                <?php foreach ($criteria_marks as $criterion => $mark): ?>
                    <tr>
                        <td><?= $criterion ?></td>
                        <td>
                            <?php for ($i = 0; $i <= 4; $i++): ?>
                                <?php if ($i <= $mark): ?>
                                    <i class="fa fa-fw fa-star"></i>
                                <?php else: ?>
                                    <i class="fa fa-fw fa-star-o"></i>
                                <?php endif; ?>

                            <?php endfor; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </table>
    </div>
</div>
