<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WorkSubtypes */

$this->title = 'Создание подвида работ';
$this->params['breadcrumbs'][] = ['label' => 'Подвиды работ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-subtypes-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
