<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkSubtypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подвиды работ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-subtypes-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Добавить подвид работ', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [

                'id',
                'name',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
