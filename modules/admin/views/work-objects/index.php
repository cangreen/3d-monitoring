<?php

use app\models\WorkObjects;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkObjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $filterData array */

$this->title = 'Объекты обработки';

?>
<div class="work-objects-index box box-primary">
  <div class="box-header with-border">
    <?= Html::a('Добавить объект обработки', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
  </div>
  <div class="box-body table-responsive no-padding">
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'layout' => "{items}\n{summary}\n{pager}",
      'columns' => [

        'id',
        'name',
        [
          'attribute' => 'examination_type_id',
          'filter' => $filterData['examinationTypes'],
          'value' => function (WorkObjects $model) {
            return $model->examinationTypeName;
          }
        ],
        [
          'attribute' => 'work_type_id',
          'filter' => $filterData['workTypes'],
          'value' => function (WorkObjects $model) {
            return $model->workTypeName;
          }
        ],
        [
          'attribute' => 'work_subtype_id',
          'filter' => $filterData['workSubTypes'],
          'value' => function (WorkObjects $model) {
            return $model->workSubtypeName;
          }
        ],

        ['class' => 'yii\grid\ActionColumn'],
      ],
    ]); ?>
  </div>
</div>
