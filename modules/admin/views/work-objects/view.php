<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WorkObjects */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Work Objects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-objects-view box box-primary">
    <div class="box-header">
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class'
      => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'examinationTypeName',
                'workTypeName',
                'workSubtypeName',
            ],
        ]) ?>
    </div>
</div>
