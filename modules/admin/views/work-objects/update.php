<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkObjects */
/* @var $filterData array */

$this->title = 'Редактирование объекта обработки: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Объекты обработки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="work-objects-update">

  <?= $this->render('_form', [
    'model' => $model,
    'filterData' => $filterData
  ]) ?>

</div>
