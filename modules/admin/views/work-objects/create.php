<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WorkObjects */
/* @var $filterData array */

$this->title = 'Создание объекта обработки';
$this->params['breadcrumbs'][] = ['label' => 'Объекты обработки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-objects-create">

  <?= $this->render('_form', [
    'model' => $model,
    'filterData' => $filterData

  ]) ?>

</div>
