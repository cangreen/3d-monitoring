<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WorkObjects */
/* @var $form yii\widgets\ActiveForm */
/* @var $filterData array */
?>

<div class="work-objects-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'examination_type_id')->dropDownList($filterData['examinationTypes']) ?>

        <?= $form->field($model, 'work_type_id')->dropDownList($filterData['workTypes'])  ?>

        <?= $form->field($model, 'work_subtype_id')->dropDownList($filterData['workSubTypes'])  ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success
      btn-flat']) ?>
      <?= Html::a('Отменить', '/work-objects/index',
      ['class' => 'btn
      btn-warning
      btn-flat']) ?>

    </div>
    <?php ActiveForm::end(); ?>
</div>
