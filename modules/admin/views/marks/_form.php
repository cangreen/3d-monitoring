<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Marks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marks-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'value')->radioList(
            [
                0 => 0,
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4
            ],
            [
                'item' => function ($index, $label, $name, $checked, $value) {

                    $return = '<div class="form_radio_btn">';
                    $return .= '<input id="marks-radio-' . $index . '" type="radio" name="Marks[value]" value="' . $value . '" tabindex="3" ';
                    if($checked) $return .= 'checked';
                    $return .= '>';
                    $return .= '<label for="marks-radio-' . $index . '">' . ucwords($label) . '</label>';
                    $return .= '</div>';

                    return $return;
                }
            ]) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success
      btn-flat']) ?>
        <?= Html::a('Отменить', '/marks/index',
            ['class' => 'btn
      btn-warning
      btn-flat']) ?>

    </div>
    <?php ActiveForm::end(); ?>
</div>
