<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Marks */

$this->title = 'Редактирование оценки: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Оценки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="marks-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
