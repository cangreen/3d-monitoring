<?php

use app\models\Marks;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MarksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Оценки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marks-index box box-primary">
    <div class="box-body table-responsive no-padding">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                [
                    'attribute' => 'checklist_id',
                    'value' => function (Marks $model) {
                        return Html::a($model->checklist_id, Url::to([
                            '/checklists/view',
                            'id' => $model->checklist_id
                        ]));
                    },
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'checklistDate',
                    'value' => function (Marks $model) {
                        return Yii::$app->formatter->asDate($model->checklistDate);
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'checklistDate',
                        'language' => 'ru',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'format' => 'dd.mm.yyyy',
                        ],
                    ]),

                ],
                [
                    'attribute' => 'criterionName',
                    'value' => function (Marks $model) {
                        return Html::a($model->criterionName, Url::to([
                            '/mark-criteria/view',
                            'id' => $model->criterion_id
                        ]));
                    },
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'value',
                    'filter' => [
                        0 => 0,
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                    ],
                    'value' => function (Marks $model) {
                        $html = '';
                        for ($i = 1; $i <= 4; $i++) {
                            $html .= $i <= $model->value ? '<i class="fa fa-fw fa-star"></i>' : '<i class="fa fa-fw fa-star-o"></i>';
                        }
                        return $html;
                    },
                    'format' => 'raw'
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
