<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Marks */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Оценки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marks-view box box-primary">
    <div class="box-header">
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class'
      => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'checklist_id',
                    'value' => Html::a($model->checklist_id, Url::to(['checklists/view', 'id' => $model->checklist_id])),
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'criterionName',
                    'value' => Html::a($model->criterionName, Url::to(['checklists/view', 'id' => $model->criterion_id])),
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'value',
                    'value' => function($model){
                        $html = '';
                        for ($i = 1; $i <= 4; $i++) {
                            $html .= $i <= $model->value ? '<i class="fa fa-fw fa-star"></i>' : '<i class="fa fa-fw fa-star-o"></i>';
                        }
                        return $html;
                    },
                    'format' => 'raw'
                ],
            ],
        ]) ?>
    </div>
</div>
