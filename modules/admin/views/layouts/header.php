<?php
use yii\helpers\Html;

$user = Yii::$app->user->identity;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $user \app\models\User */


?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">3D</span><span class="logo-lg" style="font-size: 17px">' . Yii::$app->name .
      '</span>',
      Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
              
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?= $user->email;?></span>
                    </a>
                    <ul class="dropdown-menu">

                        <li class="user-header" style="height: auto;">
                            <p>
                                <?= $user->name ?>
                              <?= $user->patronymic ?>
                              <?= $user->surname ?> <br> - <?= $roleDesc ?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/" class="btn btn-default btn-flat">Профиль</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Выход',
                                    ['/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
