<aside class="main-sidebar">
  <section class="sidebar">
    <?php if ($user->can('personal_area')): ?>
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left info" style="position: static;">
          <p><?= $roleDesc ?></p>
          <p><?= $station ?></p>
        </div>
      </div>

      <?= dmstr\widgets\Menu::widget(
        [
          'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
          'items' => [
            ['label' => 'Меню', 'options' => ['class' => 'header']],
            ['label' => 'Пользователи', 'icon' => 'user-plus', 'url' => ['/user'], 'visible' => Yii::$app->user->can('superadmin')],
            ['label' => 'Список обходных листов', 'icon' => 'table', 'url' => ['/checklists']],
            ['label' => 'Добавить обходной лист', 'icon' => 'book', 'url' => ['/checklists/create']],
            ['label' => 'Выгрузка данных', 'icon' => 'th', 'url' => ['/export'], 'visible' => !Yii::$app->user->can('operator')],
            ['label' => 'Настройки', 'icon' => 'cog', 'url' => ['/settings'], 'visible' => Yii::$app->user->can('superadmin')],
            ['label' => 'Справочник', 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->can('catalog')],
            ['label' => 'Дезстанции', 'icon' => 'building', 'url' => ['/stations'], 'visible' => Yii::$app->user->can
            ('superadmin')],
            ['label' => 'Заказчики', 'icon' => 'rub', 'url' => ['#'], 'items' => [
              ['label' => 'Заказчики', 'icon' => 'rub', 'url' => ['/customers'],],
              ['label' => 'Категории заказчиков', 'icon' => 'bars', 'url' => ['/customer-categories'],],
            ], 'visible' => Yii::$app->user->can('catalog')],
            ['label' => 'Сотрудники и бригады', 'icon' => 'group', 'url' => ['#'], 'visible' => Yii::$app->user->can('catalog'),
                'items' => [
                    ['label' => 'Сотрудники', 'icon' => 'user', 'url' => ['/employees'],],
                    ['label' => 'Бригады', 'icon' => 'group', 'url' => ['/brigades'],],
                ]],
            ['label' => 'Работы', 'icon' => 'briefcase', 'url' => ['#'], 'visible' => Yii::$app->user->can('superadmin'),
              'items' => [
                ['label' => 'Виды обследования', 'icon' => 'search', 'url' => ['/examination-types'],],
                ['label' => 'Виды работ', 'icon' => 'folder-open', 'url' => ['/work-types'],],
                ['label' => 'Подвиды работ', 'icon' => 'folder-open-o', 'url' => ['/work-subtypes'],],
                ['label' => 'Объекты обработки', 'icon' => 'building-o', 'url' => ['/work-objects'],]
            ]],
            ['label' => 'Оценки', 'icon' => 'pencil', 'url' => ['#'], 'visible' => Yii::$app->user->can('catalog'),
              'items' => [
                ['label' => 'Критерии оценки', 'icon' => 'list', 'url' => ['/mark-criteria'], 'visible' => Yii::$app->user->can('superadmin')],
                ['label' => 'Оценки', 'icon' => 'pencil-square', 'url' => ['/marks'],],
            ]],
          ],
        ]
      ) ?>
    <?php endif; ?>

  </section>

</aside>
