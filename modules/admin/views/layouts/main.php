<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$user = Yii::$app->getUser();
$roleDesc = \app\models\AuthItem::findOne(['name' => $user->identity->role])->description;
$station  = $user->identity->stationName;

dmstr\web\AdminLteAsset::register($this);
\app\assets\AdminAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">

  <?= $this->render(
    'header.php',
    [
      'user' => $user,
      'roleDesc' => $roleDesc,
      'station' => $station
    ]
  ) ?>

  <?= $this->render(
    'left.php',
    [
      'user' => $user,
      'roleDesc' => $roleDesc,
      'station' => $station
    ]
  )
  ?>

  <?= $this->render(
    'content.php',
    ['content' => $content]
  ) ?>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<?php ?>
