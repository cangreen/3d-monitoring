<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $stations array */
/* @var $roles array */

$this->title = 'Создание пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="users-create">
  <style>
    .select2-container--krajee .select2-selection {
      border-radius: unset!important;
    }
  </style>
  <div class="users-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    
    <div class="box-body table-responsive">
      
      <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'role')->dropDownList($roles, [
        'class' => 'form-control user-role-input'
      ]) ?>

      <?= $form->field($model, 'station_id')->widget(Select2::classname(), [
        'data' => $stations,
        'options' => ['placeholder' => 'Выберите дезстанцию ...', 'class' => 'user-object-input'],
        'pluginOptions' => [
          'allowClear' => true
        ],
      ]);
      ?>

    </div>
    <div class="box-footer">
      <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat']) ?>
      <?= Html::a('Отменить', '/user/index',
        ['class' => 'btn
      btn-warning
      btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
  </div>
</div>

<?php $this->registerJsFile('/js/userObject.js', ['depends' => 'yii\web\JqueryAsset'])?>

