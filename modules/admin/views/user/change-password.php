<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Смена пароля пользователя: ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'смена пароля';
?>
<div class="users-change-password">

  <div class="users-form box box-primary">
      <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= Html::passwordInput('password', '', [
            'class' => 'form-control'
        ])?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
      <?php ActiveForm::end(); ?>
  </div>

</div>

