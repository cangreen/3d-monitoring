<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
use app\models\Stations;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $roles array */
/* @var $stations array */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">
  <div class="box-header with-border">
    <?= Html::a('Добавить Пользователя', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
  </div>
  <div class="box-body table-responsive no-padding">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'layout' => "{items}\n{summary}\n{pager}",
      'columns' => [
        'id',
        'email:email',
        'name',
        'surname',
        'patronymic',
        'phone',
        [
          'attribute' => 'station_id',
          'filter' => $stations,
          'value' => function(User $model) {return $model->stationName;}
        ],
        [
          'attribute' => 'role',
          'value' => function (User $model) {
            return $model->getRoleDescription();
          },
          'filter' => $roles
        ],
        [
          'attribute' => 'active',
          'label' => 'Активен',
          'filter' => [
            0 => 'Нет',
            1 => 'Да',
          ],
          'value' => function (User $model) {
            return $model->active
              ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>'
              : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
          },
          'format' => 'raw'
        ],
        [
          'class' => 'yii\grid\ActionColumn',
          'template' => '{view} {update} {delete} {change-password}',
          'buttons' => [
            'change-password' => function ($url) {
              return Html::a(
                '<span class="glyphicon glyphicon-lock" aria-hidden="true"></span>',
                $url,
                ['title' => 'Сменить пароль']
              );
            }
          ]
        ],
      ],
    ]); ?>
  </div>
</div>
