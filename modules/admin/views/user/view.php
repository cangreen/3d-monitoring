<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-view box box-primary">
  <div class="box-header">
    <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class'
    => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger btn-flat',
      'data' => [
        'confirm' => 'Вы действительно хотите удалить?',
        'method' => 'post',
      ],
    ]) ?>
  </div>
  <div class="box-body table-responsive no-padding">
    <?= DetailView::widget([
      'model' => $model,
      'attributes' => [
        'id',
        'email:email',
        'auth_key',
        'name',
        'surname',
        'patronymic',
        'phone',
        'roleDescription',
        'active',
        [
          'attribute' => 'created_at',
          'value' => function (\app\models\User $model) {
            return Yii::$app->formatter->asDatetime($model->created_at, 'php:d.m.Y H:i:s');
          },
          'format' => 'raw'
        ],
        [
          'attribute' => 'updated_at',
          'value' => function (\app\models\User $model) {
            return Yii::$app->formatter->asDatetime($model->updated_at, 'php:d.m.Y H:i:s');
          },
          'format' => 'raw'
        ],
        'station_id',
      ],
    ]) ?>
  </div>
</div>
