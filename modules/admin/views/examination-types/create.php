<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExaminationTypes */

$this->title = 'Создание вида обследования';
$this->params['breadcrumbs'][] = ['label' => 'Виды обследования', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examination-types-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
