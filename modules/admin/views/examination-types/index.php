<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ExaminationTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Виды обследования';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examination-types-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Добавить вид обследования', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [

                'id',
                'name',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
