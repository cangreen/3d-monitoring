<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ExaminationTypes */

$this->title = 'Редактирование вида обследования: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Виды обследования', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="examination-types-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
