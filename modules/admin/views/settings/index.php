<?php

use app\models\Marks;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MarksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $default_mark \app\models\Settings */

$this->title = 'Настройки приложения';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="marks-index box box-primary">
    <div class="box-body">

        <?php if (isset($default_mark)): ?>

            <?php $defaultMarkForm = \yii\widgets\ActiveForm::begin(); ?>

            <?= $defaultMarkForm->field($default_mark, 'value')->dropDownList(
                [
                    0 => '0',
                    1 => '1',
                    2 => '2',
                    3 => '3',
                    4 => '4',
                ], [
                'prompt' => '- Нет -',
                'name' => 'Settings[default_mark]'
            ])
                ->label($default_mark->alias) ?>

            <?= Html::submitButton('Сохранить') ?>

            <?php $defaultMarkForm::end(); ?>

        <?php endif; ?>

    </div>
</div>
