<?php

use app\models\CustomerCategories;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use corpsepk\DaData\SuggestionsWidget;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\Customers */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<?php $categories = ArrayHelper::map(CustomerCategories::find()->all(), 'id', 'name')?>

<div class="customers-form box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Поиск по организациям</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">
    <?= SuggestionsWidget::widget([
      'name' => 'address-search',
      'type' => 'PARTY',
      'inputOptions' => [
        'placeholder' => 'Введите название / ИНН',
        'onSelect' => null,
      ]
    ]) ?>
  </div>
</div>
<div class="customers-form box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Основная информация</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-md-6"> <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-6"> <?= $form->field($model, 'category_id')->widget('kartik\widgets\Select2', [
          'data' => $categories,
          'options' => ['placeholder' => 'Выберите категорию ...'],
          'pluginOptions' => [
            'allowClear' => true
          ],
        ]) ?></div>
    </div>
    <?php $model->active = 1 ?>
    <?= $form->field($model, 'active')->widget(SwitchInput::class, [
      'pluginOptions' => [
        'onText' => '<i class="glyphicon glyphicon-ok"></i>',
        'offText' => '<i class="glyphicon glyphicon-remove"></i>',
      ]
    ]) ?>

  </div>
</div>
<div class="customers-form box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Реквизиты</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">

    <div class="row" style="margin-top: 20px;">
      <div class="col-md-4"><?= $form->field($model, 'tax_number')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-4"><?= $form->field($model, 'index')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-4"><?= $form->field($model, 'city')->textInput() ?></div>
    </div>
    <div class="row">
      <div class="col-md-3"> <?= $form->field($model, 'district')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-3"> <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-3"> <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-2"><?= $form->field($model, 'house')->textInput(['maxlength' => true]) ?></div>
      <div class="col-md-1"><?= $form->field($model, 'flat')->textInput(['maxlength' => true]) ?></div>
    </div>
  </div>
</div>
<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success
      btn-flat']) ?>

<?= Html::a('Отменить', '/customers/index',
  ['class' => 'btn
      btn-warning
      btn-flat']) ?>
<?php ActiveForm::end(); ?>

<?php $this->registerJsFile('/js/ddataCustomersForm.js', ['depends' => 'yii\web\JqueryAsset']) ?>

