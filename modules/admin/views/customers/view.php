<?php

use app\models\Customers;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Customers */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Заказчики', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->tax_number;
?>
<div class="customers-view box box-primary">
    <div class="box-header">
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class'
      => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'categoryName',
                'name',
                'tax_number',
                'index',
                'city',
                'district',
                'region',
                'street',
                'house',
                'flat',
                'active',
              [
                'attribute' => 'created_at',
                'value' => function (Customers $model) {
                  return Yii::$app->formatter->asDatetime($model->created_at, 'php:d.m.Y H:i:s');
                },
                'format' => 'raw'
              ],
              [
                'attribute' => 'updated_at',
                'value' => function (Customers $model) {
                  return Yii::$app->formatter->asDatetime($model->updated_at, 'php:d.m.Y H:i:s');
                },
                'format' => 'raw'
              ],
            ],
        ]) ?>
    </div>
</div>
