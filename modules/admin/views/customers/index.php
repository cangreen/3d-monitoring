<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Customers;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказчики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customers-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Добавить Заказчика', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'id',
                'categoryName',
                'name',
                'tax_number',
//                'index',
                // 'city',
                // 'district',
                // 'region',
                // 'street',
                // 'house',
                // 'flat',
              [
                'attribute' => 'active',
                'label' => 'Активна',
                'filter' => [
                  0 => 'Нет',
                  1 => 'Да',
                ],
                'value' => function(Customers $model) {
                  return $model->active
                    ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>'
                    : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
                },
                'format' => 'raw'
              ],
//                 'created_at',
//                 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
