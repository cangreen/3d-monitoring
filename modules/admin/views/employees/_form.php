<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $form yii\widgets\ActiveForm */
/* @var $brigades array */
?>

<div class="employees-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'brigade_id')->widget('\kartik\select2\Select2', [
                'data' => $brigades,
                'options' => ['placeholder' => 'Выберите бригаду...'],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]
        ) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success
      btn-flat']) ?>
      <?= Html::a('Отменить', '/employees/index',
      ['class' => 'btn
      btn-warning
      btn-flat']) ?>

    </div>
    <?php ActiveForm::end(); ?>
</div>
