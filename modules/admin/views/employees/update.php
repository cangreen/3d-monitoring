<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $brigades array */

$this->title = "Редактирование работника: {$model->surname} {$model->name} {$model->patronymic}";
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="employees-update">

    <?= $this->render('_form', [
        'model' => $model,
        'brigades' => $brigades
    ]) ?>

</div>
