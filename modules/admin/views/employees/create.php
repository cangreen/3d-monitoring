<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $brigades array */

$this->title = 'Добавление работника';
$this->params['breadcrumbs'][] = ['label' => 'Работники', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-create">

    <?= $this->render('_form', [
        'model' => $model,
        'brigades' => $brigades
    ]) ?>

</div>
