<?php


namespace app\modules\api\controllers;

use app\models\WorkObjects;
use Yii;
use yii\helpers\Json;
use yii\rest\ActiveController;

class WorkObjectsController extends ActiveController
{
    public $modelClass = 'app\models\WorkObjects';


    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => 'yii\filters\Cors',
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Request-Headers' => ['*'],
                ],
            ]
        ];
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['delete'], $actions['create'], $actions['update']);
    }

    public function actionIndex($examination_type_id, $work_type_id, $work_subtype_id)
    {
        $response = [];

        $objects = WorkObjects::find()->where([
            'examination_type_id' => $examination_type_id,
            'work_type_id' => $work_type_id,
            'work_subtype_id' => $work_subtype_id
        ])->all();


        foreach ($objects as $object) {
            $response[$object->id] = [
                'name' => $object->name,
                'criteria' => $object->getMarkCriterias()->all()
            ];
        }

        Yii::$app->response->data = Json::encode([
            'status' => true,
            'error' => null,
            'default' => Yii::$app->settings->default_mark,
            'data' => $response
        ]);
    }
}
