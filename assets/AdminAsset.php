<?php


namespace app\assets;


class AdminAsset extends AppAsset
{
  public $css = [
    'css/custom.css',
  ];

}