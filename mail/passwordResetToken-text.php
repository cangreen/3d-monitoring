<?php

/**
 * @var $user \app\models\User
 */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/reset-password', 'token' => $user->password_reset_token]);
?>

  Здравствуйте, <?= $user->email ?>,
  Для сброса пароля пройдите по ссылке ниже:

<?= $resetLink ?>