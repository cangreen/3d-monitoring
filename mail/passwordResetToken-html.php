<?php

use yii\helpers\Html;

/**
 * @var $user \app\models\User
 */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/reset-password', 'token' => $user->password_reset_token]);
?>

<div class="password-reset">
  <p>Здравствуйте, <?= Html::encode($user->email) ?>,</p>
  <p>Для сброса пароля пройдите по ссылке ниже:</p>
  <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>