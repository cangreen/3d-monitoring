<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\seeders\BrigadesSeeder;
use app\seeders\Seeder;
use SebastianBergmann\CodeCoverage\Report\PHP;
use yii\console\Controller;
use yii\console\ExitCode;
use app\seeders\StationsSeeder;
use yii\helpers\Json;


class SeedController extends Controller
{
    /**
     * @var string
     */
    protected $namespace = 'app\\seeders\\';

    public $className;

    protected $seeders = [
        StationsSeeder::class,
        BrigadesSeeder::class
    ];

    /**
     * Seeds data. Option: className  (--className , -c)
     */
    public function actionIndex()
    {
        if (is_null($this->className)) {
            foreach ($this->seeders as $seederClass) {
                $seeder = (new $seederClass());
                $result = $seeder->init();
                echo "$seederClass Models created: {$result['models']}" . PHP_EOL;
                echo count($result['errors']) === 0
                    ? "$seederClass Errors: 0" . PHP_EOL
                    : "Error: " . Json::encode($result['errors'][0]) . PHP_EOL;
            }
        } else {
            $class = $this->namespace . $this->className;
            $seeder = new $class();
            $result = $seeder->init();
            echo "$this->className Models created: {$result['models']}" . PHP_EOL;
            echo count($result['errors']) === 0
                ? "$this->className Errors: 0" . PHP_EOL
                : "Error: " . Json::encode($result['errors'][0]) . PHP_EOL;
        }
    }

    /**
     * @param string $actionIndex
     * @return string[]
     */
    public function options($actionIndex)
    {
        return ['className'];
    }

    /**
     * @return array|string[]
     */
    public function optionAliases()
    {
        return [
            'c' => 'className'
        ];
    }


}
