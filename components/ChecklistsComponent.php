<?php


namespace app\components;

use app\models\Checklists;
use Yii;


class ChecklistsComponent
{
    /**@property Checklists $checklist */

    private $checklist;

    /**
     * ChecklistsComponent constructor.
     */
    public function __construct()
    {
        $this->checklist = new Checklists();
        $this->init();
    }

    /**
     *
     */
    public function init()
    {
        $this->checklist->created_at = date('U');

        $info = Yii::$app->session->get('info');
        if(!empty($info)){
            $this->checklist->load(unserialize($info), '');
        }

        if (!Yii::$app->user->can('superadmin')) {
            $this->checklist->station_id = Yii::$app->user->identity->station_id;
        }
    }


    /**
     * @return Checklists
     */
    public function getChecklist(): Checklists
    {
        return $this->checklist;
    }

}