<?php


namespace app\components;


use app\models\Checklists;

class SessionComponent
{
    private $checklist;

    public function __construct(Checklists $model)
    {
        $this->checklist = $model;
    }

    public function saveInfo(): void
    {
        \Yii::$app->session->set('info', serialize([
            'examination_type_id' => $this->checklist->examination_type_id,
            'customer_id' => $this->checklist->customer_id,
            'station_id' => $this->checklist->station_id,
            'brigade_id' => $this->checklist->brigade_id,
            'work_type_id' => $this->checklist->work_type_id,
            'work_subtype_id' => $this->checklist->work_subtype_id
        ]));
    }

}