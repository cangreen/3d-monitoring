<?php


namespace app\components;


use yii\base\Component;
use yii\helpers\ArrayHelper;
use app\models\Settings;

class SettingsComponent extends Component
{
    private $_attributes;

    public function init()
    {
        parent::init();
        $this->_attributes = ArrayHelper::map(Settings::find()->all(), 'name', 'value');
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->_attributes))
            return $this->_attributes[$name];

        return parent::__get($name);
    }


}